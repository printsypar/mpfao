% !TEX root = ./fvca7-IlievEtAl.tex
\section{Space discretization using MPFA}
\label{sec:mpfa}
The equations are discretized using the cell-centered collocated finite volume approach on quadrilaterals. Due to the complex quadrilateral grid arrangement and varying (discontinuous) coefficients in the Stokes-Brinkman case, special attention is paid to the spatial discretization of the terms. The Multipoint Flux Approximation method (MPFA) (see \cite{Aavatsmark2002,Aavatsmark2007}) is employed to approximate the following type of terms $\int_{v} \nabla \cdot \alpha \nabla \phi d\mathbf{x}$ and $\int_{v} \nabla \phi d\mathbf{x}$, where $\phi$ is a scalar function representing one of the velocity components or the pressure, $\alpha$ is a constant coefficient in our case, but this discretization technique can account for a full tensor coefficient $\alpha$. 

At first, let us describe some necessary details. We employ two grids, name primary and dual grids.  As shown in Fig.~\ref{fig:grid}, the primary grid consists of quadrilaterals marked in red. The dual grid marked in blue is formed by connecting the centers of primary quadrilaterals and the midpoints of their edges. The duals are further divided into four parts, each part belongs to a different quadrilateral. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.6\textwidth]{pics/grid.png}
	\caption{Primary and dual grids}
	\label{fig:grid} 
\end{figure}

%
\emph{Approximation of %$-\int_{v_i} \nabla \cdot \alpha \nabla \phi_i dV$}
$-\int_{v} \nabla \cdot \alpha \nabla \phi d\mathbf{x}$}

Employing the Gauss' divergence theorem, we get the flux $f_v$ through the boundary $\partial v$ of the control volume $v$
\begin{equation}
	\label{eq:flux}
	f_v = -\int_{v} \nabla \cdot \alpha \nabla \phi d\mathbf{x} 
	= -\oint_{\partial v} (\alpha \nabla \phi) \cdot \normal \, dl.
\end{equation}
The evaluation of the flux is reduced to calculation of the integral in equation \eqref{eq:flux}, which is standard for the MPFA method (see \cite{Aavatsmark2002}). We can write a flux expression for the quadrilateral $v$ as follows
\begin{equation}
	\label{eq:mpfaFlux}
	f_v = \sum_{i=0}^{n_e-1}f_i 
	\approx \sum_{i=0}^{n_e-1}\left(\sum_{j=0}^{n_p-1}t_{ij}\hat{\phi}_j\right).
\end{equation}
As shown in Fig.~\ref{fig:grid}, 
	$f_i$ is the flux through half edges $e_i$
	with $n_e=8$ denoting the number of half edges of the primary cell and
	$n_p=4$ denoting the number of primary quadrilaterals in the dual cell,
	$t_{ij}$ is called the transmissibility coefficient, 
	computed via the MPFA O-method (see equation (26) in \cite{Aavatsmark2002}).
$\hat{\phi}_j$ is the value of function $\phi$ at the center of the $j$th primary quadrilateral in the dual cell (see Fig.~\ref{fig:grid}), which also represent unknowns in the discretized system.

%
\emph{Approximation of $\int_{v} \nabla \phi d\mathbf{x}$}

Here the MPFA method is extended in order to approximate the gradient operators in the last terms of equations \eqref{Chorin-ustar} and \eqref{Chorin-velupdate1}. Using the Green's theorem and fundamental relations between ordinary and line integrals, for the first gradient component we obtain
\begin{equation} 
	\label{Green}
	\begin{split}
	\int_{v} \frac{\partial \phi}{\partial x} d\mathbf{x} 
	& = \oint_{\partial v} 0dx + \phi dy
	= \oint_{\partial v} (\phi, 0)\cdot (dy, -dx) 
	= \oint_{\partial v} (\phi, 0)\cdot \mathbf{n} dl \\
	& = \oint_{\partial v} \phi n_x dl 
	\approx \sum_{i=0}^{n_e-1}\phi_i s_i n_{x,i}.
	\end{split}
\end{equation}
where 
	$\phi_i$ are the values of $\phi$ on $e_i$ (see Fig.~\ref{fig:grid}),
	$s_i$ is the measure of $e_i$, $s_i=mes(e_i)$,
	$\mathbf{n}_i=(n_{x,i},n_{y,i})$ is the outward unit normal vector of $e_i$.  
Values $\phi_i$ can also be estimated using MPFA method like fluxes $f_i$ in \eqref{eq:mpfaFlux} using values $\hat{\phi}_j$ but with different transmissibility coefficients (see equation (24) in \cite{Aavatsmark2002}). Similar procedure can be carried out to approximate the second component of the gradient $\int_v\partial \phi/\partial y d\mathbf{x}$.
