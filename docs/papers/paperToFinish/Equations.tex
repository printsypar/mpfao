% !TEX root = ./fvca7-name-template.tex
\section{Modeling of coupled free and porous media flow}
\label{ssec-Fluid-equations}
There exist different models for simulating a free fluid flow coupled with a flow in porous media. One of the popular approaches is to use Stokes and Darcy flow problems with interface coupling conditions. Another approach is to use the Navier-Stokes-Brinkman model that had been presented for the fluid flow through a filter element in our earlier work (see \cite{CIL2007} and references therein). In this study we are concerned with a reduced model using the Stokes-Brinkman equations, which read
%
\begin{equation}
	\label{eq:SB}
	\begin{split}
	\dens\dfrac{\partial\vel}{\partial t} - \nabla\cdot (\visc\nabla\vel)
	+ \visc\perm^{-1}\vel&= -\nabla p\,,\quad\mathbf x\in\Omega,\\
	\nabla\cdot\vel &= 0\,,\quad\mathbf x\in\Omega.
	\end{split}
\end{equation}
%
Here $\vel$ and $p$  denote the fluid velocity vector and the fluid pressure respectively. Moreover, $\visc$ is the fluid dynamic viscosity. $\Omega$ is the computational domain, which consists of two nonintersecting subsets, namely the fluid domain $\Omega_f$ and the porous domain $\Omega_p$. $\perm$ is the intrinsic permeability of the porous medium in $\Omega_p$ and $\perm^{-1}=0$ in $\Omega_f$.

A typical set of boundary conditions looks as follows
\begin{equation}
	\label{sms:eq_BC_Vel_NSB_inlet}
	\vel(\mathbf x)=\vel_{\text{in}}(\mathbf x),\; \mathbf x\in\,\bdry_i; \ \ \ \ \
	\mathbf{\sigma}\cdot\mathbf{n}=0,
	\;\mathbf x\in\,\bdry_o; \ \ \ \ \
	\vel(\mathbf x)=0,\;\mathbf x\in\,\bdry_s;
\end{equation}
where $\bdry_i$, $\bdry_o$ and $\bdry_s$ denote the inlet, outlet and solid wall boundaries, $\mathbf{\sigma}$ is the stress tensor, $\mathbf{n}$ is the outward unit normal to $\bdry_o$. More details can be found, for example, in \cite{CIL2007,Laptev2004}.

\begin{comment}
\subsubsection{Linear systems and its solution}
Each iteration of the Chorin-type algorithm, particularly Equations \ref{Chorin-ustar} and \ref{Chorin-pc}, involves solving systems of 
sparse, unsysmmetric linear equations (for $\vel^\ast$, and $p^{'}$)
\begin{equation}
\label{eq:linearsystem}
\mathcal{A} \mathbf{\phi} = \mathbf{b},
\end{equation}
where $\mathcal{A}$ is the discretization matrix, $\mathbf{\phi}$ is the vector of unknowns, and $\mathbf{b}$ is the right hand side vector. 
The BiCGStab algorithm or the SAMG algorithm is employed for solving such linear systems. 
The equations are discretized using the cell-centered collocated finite volume approach on quadrilaterals. Due to the complex quadrilateral grid 
arrangement and varying (discontinuous) coefficients in the Stokes-Brinkman case, special attention is paid to the spatial discretization of the terms. 
The Multipoint Flux Approximation (MPFA) method is employed to approximate the $\int_{v_i} \nabla \cdot \alpha \nabla \phi_i dV$ and $\int_{v_i} \nabla \phi_i dV$ terms.
This will be described in the following subsections.

\subsection{Discretization using MPFA}
For the approximation of this term, we first describe some necessary details. We employ two grids, name primary and dual grids.  
The primary grid is comprised of quadrilaterals. The dual is a staggered-type grid which is constructed by connecting the centres 
of each quadrilateral in the primary grid. See figure \ref{fig:quadrisduals}.

\begin{comment}
\begin{figure}[h!]
\centering
\label{fig:quadrisduals} 
\includegraphics[width=0.5\textwidth]{quadrisduals}
\caption{Figure illustrates the two grids, primary and dual. 
The primary grid consists of the green and red outlined quadrilaterals. 
The dual grid is formed by connecting the centres of primary quadrilaterals and the midpoints of their edges.
In the discussions, the quadrilateral marked in red will be referred to the control volume under consideration. 
The discretization of the terms is described for this control volume. The duals are further divided into four parts, 
each part belongs to a different quadrilateral. $n_i$ denote the indices of the quadrilaterals.}
\end{figure}
\end{comment}

\begin{comment}
\subsubsection*{Approximation of %$-\int_{v_i} \nabla \cdot \alpha \nabla \phi_i dV$}
%% GP
$-\int_{v} \nabla \cdot \alpha \nabla \phi dV$}
Employing the Gauss' divergence theorem to the control volume formulation of $-\int_{v} \nabla \cdot \alpha \nabla \phi dV$, 
we get the flux, $f$, through the surface $S$ of the control volume $v$
\begin{equation}
\label{eq:flux}
f = -\oint_{S} (\alpha \nabla \phi) \cdot \normal \, dS.
\end{equation}
%%
%Employing the Gauss' divergence theorem to the control volume formulation of $-\int_{v_i} \nabla \cdot \alpha \nabla \phi_i dV$, 
%we get the flux, $f_i$, through the surface $S_i$ of the control volume 
%\begin{equation}
%\label{eq:flux}
%f_i = -\oint_{S_i} (\alpha \nabla \phi_i) \cdot \normal \, dS.
%\end{equation}
%The evaluation of the flux is reduced to calculation of integral in Equation \ref{eq:flux}. 
%Here, the fluxes for each internal edge of a dual cell (as shown in Figure \ref{fig:dualwithQuadris}) 
%and are approximated by a multipoint flux expression
%% GP
The evaluation of the flux is reduced to calculation of integral in Equation \ref{eq:flux}. 
Here, the fluxes through half edges $e_i$ are denoted $f_i$ for $i=0,\ldots,n_e-1$ (as shown in Figure \ref{fig:dualwithQuadris}) and approximated by a multipoint flux expression
\begin{equation}
\label{eq:mpfaFlux}
f_{i}=\sum_{j=0}^{n_p-1}t_{ij}\phi_j+\sum_{j=0}^{n_p-1}c_{ij},
\end{equation} 
where the $n_p$ denotes the number of primary quadrilaterals in the dual cell and 
	$n_e$ denotes the number of edges inside the dual cell. 
	$\phi_j$ is the value of function $\phi$ at the center of the $j$th primary quadrilateral in the dual cell, 
	$t_{ij}$ and $c_{ij}$ are called the transmissibility coefficeients, computed via the MPFA O-method \cite{Aavatsmark2002,Aavatsmark2007,Eigestad}.
%%

%\begin{equation}
%\label{eq:mpfaFlux}
%f_{e}=\sum_{j=0}^{n_p}t_{ej}\phi_j+\sum_{j=0}^{n_p}c_{ej},
%\end{equation} 
%where $n_p$ denotes the number of primary quadrilaterals in the dual cell 
%and $e=0,\ldots,n_e$, where $n_e$ denotes the number of edges inside the dual cell. $t_{ej}$ are called the transmissibility coefficeients, 
%computed via the MPFA O-method \citep{Aavatsmark2002,Aavatsmark2007,Eigestad}.

%
%
%
\begin{comment}
\begin{figure}[h!]
\label{fig:dualwithQuadris}
\centering
\includegraphics[width=9cm]{centreDualwithQL}
\caption{The figure shows a dual cell comprised of 4 quadrilateral control volumes. 
Each internal edge of the dual is shared by 2 quadrilaterals.}
\end{figure}
\end{comment}
%
%
\begin{comment}
%%%%%%%%%%%% MPFA %%%%%%%%%%%%%%%%
For a quadrilateral (as showed in Figure \ref{fig:quadriFluxes}), we can write the flux equation as follows:
\begin{equation}
\label{eq:quadriFlux}
f = -\big((f_{0^+}-f_{0^-})+(f_{1^+}-f_{1^-})+(f_{2^+}-f_{2^-})+(f_{3^+}-f_{3^-})\big)
\end{equation} 

\begin{comment}
\begin{figure}[h!]
\label{fig:quadriFluxes}
\centering
\includegraphics[width=9 cm]{quadriFluxes}
\caption{The figure shows how the fluxes are organized for each primary grid quadrilateral. 
Each edge is divided into two subedges, with a certain flux direction for each subedge. }
\end{figure}
\end{comment}

\begin{comment}
\subsubsection*{Approximation of $\int_{v_i} \nabla \phi_i dV$}
The calculation of gradients is based on fundamental relations between double and line integrals.

\begin{equation} \label{Green}
 \int_{v_i} (\nabla \phi_i) dV = \oint_S \phi_i \vec {dS}.
\end{equation}

Similar dependences is valid in 3D.  Using [\ref{Green}] we can estimate the 
average gradient components over cell volume knowing values of the function itself on the cell
walls:

\begin{equation} \label{calc_grad}
\nabla \phi_i |_{v_i} = \frac{\oint_S \phi_i \vec {dS}}{mes(v_i)}.
\end{equation}

The values of $\phi_i$ on cell  faces are estimated using mpfa. In 2D the face value
can be estimated from 2 duals. Recently in [\ref{calc_grad}] the arithmetic average
from these two values is used.

\end{comment}






