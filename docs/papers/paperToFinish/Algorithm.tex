% !TEX root = ./fvca7-IlievEtAl.tex
\section{Chorin type method for Stokes-Brinkman equations}
%
\emph{Chorin type algorithm.}	
\label{ssec-Chorin}
At first, let us introduce for simplicity operators $\mathcal{D}=\nabla \cdot \mu \nabla$ and $\mathcal{B}=\mu \perm^{-1}$. Then, the system of equations \eqref{eq:SB} to be solved reads
\begin{align}
	\rho\dfrac{\partial\vel}{\partial t}- \mathcal{D}\,\vel 
	+ \mathcal{B}\,\vel&=-\nabla p,\quad\mathbf x\in\Omega,\\
	\nabla \cdot \vel&=0, \quad\mathbf x\in\Omega.
\end{align}
For a given discretization time step $\tau>0$ and an initial time moment $t_0\geq 0$, we define $t_n=t_0+n\,\tau\,,\;n=0,1,\dots$. Let $\vel^{n}$ and $p^{n}$ denote the approximation of $\vel$ and $p$ at time $t_n$. The fractional time step discretization can be written as
\begin{align}
	\label{eq:chorin1}
	\dfrac{\rho}{\tau}\big(\vel^\ast-\vel^{n}\big)-\mathcal{D}\vel^\ast 
	+ \mathcal{B} \vel^\ast &= -\nabla p^{n}\,,
	\\
	\label{eq:chorin2}
	\dfrac{\rho}{\tau}\big(\vel^{n+1}-\vel^\ast\big)
	+ \mathcal{B}\left(\vel^{n+1} - \vel^\ast\right) 
	&= -\nabla \left(p^{n+1} - p^{n}\right) \,,
	\\
	\label{eq:chorin4}
	\nabla \cdot \vel^{n+1} &= 0 \, ,
\end{align}
where $\vel^*$ is a prediction to the fluid velocity. Transforming equation \eqref{eq:chorin2}, we obtain
\begin{equation}
	\label{eq:chorin_velupdate}
	\vel^{n+1} = \vel^\ast 
	- \left(\dfrac{\rho}{\tau} + \mathcal{B}\right)^{-1} \nabla p^{'},
\end{equation}
where $p^{'} = p^{n+1}-p^{n}$ is the pressure correction.
Then, we apply a divergence operator to both sides of equation \eqref{eq:chorin_velupdate}. Using the continuity equation  $\nabla \vel^{n+1}_v = 0$, we have
\begin{equation}
	\label{eq:chorin_continuity}
	- \nabla\cdot\left(\left(\dfrac{\rho}{\tau} + \mathcal{B}\right)^{-1} \nabla p^{'}\right)
	= - \nabla \cdot \vel^\ast.
\end{equation}

%
\emph{Finite volume integral formulation.}
The computational domain $\Omega$ is subdivided into a set $\mathcal{V}$ of quadrilateral finite volumes $v$. Integrating the system of equations \eqref{eq:chorin1}, \eqref{eq:chorin_velupdate}, and \eqref{eq:chorin_continuity} over each $v$, we obtain the following Chorin type algorithm (for more details see \cite{CIL2007})
\begin{align}
	\label{Chorin-ustar}
	\int_{v}\dfrac{\rho}{\tau}\big(\vel^\ast-\vel^{n}\big) d\mathbf{x} 
	- \int_{v}\mathcal{D}\vel^\ast d\mathbf{x} + \int_{v} \mathcal{B} \vel^\ast d\mathbf{x} 
	= -\int_{v} \nabla p^{n} d\mathbf{x},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-pc}
	- \int_{v} \nabla \cdot \left( \left(\dfrac{\rho}{\tau} 
	+ \mathcal{B}\right)^{-1} \nabla p^{'}\right) d\mathbf{x} 
	= -\int_v \nabla \cdot \vel^\ast d\mathbf{x},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-velupdate1}
	\vel^{n+1}_v= \frac{1}{V}\int_{v} \left( \vel^\ast - \left(\dfrac{\rho}{\tau} 
	+\mathcal{B}\right)^{-1} \nabla p^{'} \right) d\mathbf{x},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-prupdate}
	p^{n+1}_v = p^{n}_v + p^{'}_v,
	&\quad v\in\mathcal{V}.
\end{align}
\begin{comment}
\begin{align}
	\label{eq-Chorin-intermediate1_fv}
	\int_{v}\dfrac{\rho}{\tau}\big(\vel^\ast-\vel^{n}\big) d\mathbf{x} 
	- \int_{v}\mathcal{D}\vel^\ast dV + \int_{v_i} \mathcal{B} \vel^\ast d\mathbf{x}
	=&
	-\int_{v} \nabla p^{n} d\mathbf{x},\quad v\in\mathcal{V};\\
	\label{eq-Chorin-intermediate2_fv}
	\int_{v}\dfrac{\rho}{\tau}\left(\vel^{n+1}-\vel^\ast\right) d\mathbf{x}
	+ \int_{v}\mathcal{B} (\vel^{n+1} -  \vel^\ast) d\mathbf{x}
	=&
	-\int_{v}\nabla \left(p^{n+1} - p^{n}\right) d\mathbf{x}\quad v\in\mathcal{V};\\
	\label{eq-Chorin-intermediate4_fv}
	\int_{v}\nabla \cdot \vel^{n+1} d\mathbf{x} =& 0,\quad v\in\mathcal{V}.
\end{align}
Let us now consider equations \eqref{eq-Chorin-intermediate1_fv}--\eqref{eq-Chorin-intermediate4_fv} consequently. We start with equation~\eqref{eq-Chorin-intermediate2_fv}, which yields
\begin{equation}
	\label{eq-Chorin-intermediate21_fv}
	\dfrac{\rho}{\tau}\left(\vel^{n+1}_v-\vel^\ast_v\right) V 
	+ \mathcal{B}\left(\vel^{n+1}_v -  \vel^\ast_v\right) V
	=
	-\int_{v}\nabla \left(p^{n+1} - p^{n}\right) d\mathbf{x},\quad v\in\mathcal{V};
\end{equation}
where $V=mes(v)$ is the measure of the finite volume $v$, index '$v$' denotes volume averaged variables.
Denoting pressure correction $p^{'} = p^{n+1}-p^{n}$ and rearranging equation \eqref{eq-Chorin-intermediate21_fv}, we get the velocity at time $t_{n+1}$
\begin{equation}
	\label{eq-Chorin-velupdate}
	\vel^{n+1}_v= \vel^\ast_v 
	- \frac{1}{V}\int_{v} \left( \dfrac{\rho}{\tau} +\mathcal{B} \right)^{-1} 
	\nabla p^{'} d\mathbf{x}.
\end{equation}
Then, we apply a divergence operator to both sides of equation \eqref{eq-Chorin-velupdate}. Using the continuity equation  $\nabla \vel^{n+1}_v = 0$, we have
\begin{equation}
	- \frac{1}{V} \int_{v} \nabla\cdot\left(\left(\dfrac{\rho}{\tau} + \mathcal{B}\right)^{-1}
	\nabla p^{'}\right) d\mathbf{x} = - \nabla \cdot \vel^\ast_v.
\end{equation}
\end{comment}
