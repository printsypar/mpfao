% !TEX root = ./fvca7-IlievEtAl.tex
\section{Numerical results}
MPFA is one of the discretization techniques which was developed to approximate fluxes with full tensor coefficient on irregular grids. Standard discretization techniques such as two-point flux approximation can lead to unphysical or inaccurate results for irregular grids. In this section we present a channel example and compare its solution on different regular and irregular grids along with the analytical solution. Example for a radial Stokes-Brinkman flow in a ring is also presented on irregular grid. Obtained values are compared to their analytic counterparts. Another example for Stokes- Brinkman flow in a U-shaped pleat is prsented. The particular example of the pleat is chosen based on its importance in the field of industrial filtration. The numerical solutions of the discussed algorithm are compared against the finite volume based commercial software FiltEST.  

%
\emph{Stationary channel flow between two parallel plates.}
The domain is a 2D channel of width $d$ (along $x_1$) between two infinitely large plates. The equations are considered in Cartesian coordinates. We assume that the flow is stationary ${\partial \vel}/{\partial t}\equiv 0\,$, and there is no flow in $x_1$ direction, $u_1\equiv 0$. Moreover, the pressure is linear in $x_2$ and does not depend on $x_1$, i.e. ${\partial p}/{\partial x_2}\equiv -C_p$. The boundary conditions are ''no-slip'', where 
$
\vel(x_1=0)=
\vel(x_1=d)=
0
$.
Then, in infinitely long channel the analytical solution yields 
\[ 
	u_2(x_1)=\dfrac{C_p}{2\mu}\Big(d{x_1}-x_1^2\Big)
	=
	\dfrac{\pdrop}{2\mu L}\Big(d x_1-x_1^2\Big),
\]
where $\pdrop$ is the pressure drop along the channel at distance $L$, $\mu$ is the viscosity. Then, the maximum centerline velocity reads 
\begin{equation}
	\label{uforchannel}
	u_{2,max}\left(\frac{d}{2}\right)=\frac{p_{drop}d^2}{8\mu L} 
	 = 1.5 u_{in},
\end{equation}
where $u_{in}$ is the inflow or average velocity.
%

Here, we present the results for the channel problem with a simple rectangular computational domain. The numerical algorithm was tested on different complex grids (see Fig.~\ref{qlGrids_Channel}), such as stretched Cartesian grid, fish-bone grid, and mosaic grid, with different resolutions. The inflow parameters used for this problem are the viscosity $\mu= 0.2\;kg/ms$, the inlet velocity  $u_{in} = 0.015\;m/s$, the fluid density  $\rho=800\;kg/m^3$. Dimensions of the channel are $d=1\;m$ and $L=5\;m$.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/Grids_Channel.png}
	\caption {Different quadrilateral grids: stretched Cartesian grid (left), 
	fish-bone grid (middle), and mosaic grid (right). }
	\label{qlGrids_Channel}
\end{figure}

\begin {figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/Velocities_Channel_2.png}
	\caption {Velocity distributions on different grids, i.e. stretched 
	Cartesian grid (left), fish-bone grid (middle), and mosaic shaped 
	quadrilaterals (right). Resolution is $80 \times 40$ quadrilaterals.}
	\label{qlGrids_Velocity}
\end {figure}

The velocity profiles on the different grids are illustrated in Fig.~\ref{qlGrids_Velocity}. Table~\ref{tableChannel} summarizes the results on different computational domains for different grid resolutions. According to the analytical solution \eqref{uforchannel}, the maximum centerline velocity is $u_{2,max} =  0.0225\;m/s$ and the pressure drop is $\pdrop = 0.18\;Pa$. It can be seen that the complex grids result in a comparable pressure and velocity accuracy. Moreover the results on different resolutions also compare well. It can be concluded that the algorithm works well on an arbitrary complex composition of quadrilateral grids for the Stokes system of equations.  

\begin{table}[!ht]
	\begin{center}
	\begin{tabular}{|l|l|l|l|}
	\hline 
	\bf {Geometry} & \bf{Resolution} & $p_{drop}$, $[Pa]$ & $u_{2,max}$, $[m/s]$\\
	\hline
	\multirow{2}{*}{Cartesian} & $80 \times 40$ & $0,1749$ & $0,0228$\\
	\hhline{~---}
	& $40 \times 20$ & $0,19$ & $0,02269$ \\
	\hline
	\multirow{3}{*}{fish-bone} & $80 \times 40$ & $0,1745$ & $0,0228$ \\
	\hhline{~---}
	& $80 \times 20$ & $0,1859$ & $0,02214$ \\
	\hhline{~---}
	& $40 \times 20$ & $0,185$ & $0,02268$ \\
	\hline
	\multirow{3}{*}{mosaic} & $80 \times 40$ & $0,1735$ & $0,02268$ \\
	\hhline{~---}
	& $80 \times 20$ & $0,1829$ & $0,02192$ \\
	\hhline{~---}
	& $40 \times 20$ & $0,1882$ & $0,02247$ \\
	\hline
	\end{tabular}
	\end{center}
	\caption{Summary of results on different computational domains for the flow 
	between two parallel plates. Convergence and grid study. }
	\label{tableChannel}
\end{table}

%
\emph{Radial Stokes-Brinkman flow in a ring.}
The domain is a 2D ring (annulus) which contains an additional ring-shaped porous medium. The inlet is located on the outer circle boundary and the outlet is on the inner circle boundary. They are separated by the porous medium. On the inlet and the outlet we use Dirichlet boundary conditions for the fluid velocity $u_r(R_1)=u_r^{out}$ and $u_r(R_2)=u_r^{in}$. 
Using the continuity equation, the solution for the fluid velocity is given by
\[
	u_r(r,\varphi)=u_r(r)=\dfrac{R_2\,u_r^\inlet}{r}
	\,,\;
	u_\varphi(r,\varphi)\equiv 0\,, \text{ for }r\in[R_1,R_2]\text{ and }\varphi\in[0,2\pi],
\]
where $\vel=(u_r, u_{\varphi})$ is the fluid velocity in polar coordinates $r$ and $\varphi$. All other parameters are introduced in Table~{\ref{tab:2}}. The momentum equation in the porous media in polar coordinates reads
\[
	\dfrac{\mu}{K}\dfrac{R_2\,u_r^\inlet}{r}
	=-\dfrac{\partial p}{\partial r},\; \text{ for } r\in[r_1,r_2].
\]
Then, an analytical expression for the pressure drop across the porous medium yields
\[
	p(r_2)-p(r_1)=-\dfrac{\mu}{K}R_2\,u_r^\inlet\ln\Big(\dfrac{r_2}{r_1}\Big)\,.
\]

\begin{table}[!ht]
	\begin{center}
	\begin{tabular}{|l|c|l|l|c|l|}
	\hline 
	Inner radius, $R_1$ & $[m]$ & $1$ 
	& Inflow velocity, $u_r^{in}$ & $[m/s]$ & $6.36692\times10^{-6}$ \\
	\hline
	Outer radius, $R_2$ & $[m]$ & $5$ 
	& permeability, $K$ & $[m^2]$ & $1\times10^{-12}$ \\
	\hline
	Inner porous radius, $r_1$ &$[m]$ & $2.516375$
	& fluid density, $\rho$ & $[kg/m^3]$ & $800$ \\
	\hline
	Outer porous radius, $r_2$ & $[m]$ & $3.48156$ 
	& viscosity, $\mu$ & $[kg/m s]$ & $0.2$ \\
	\hline
	\end{tabular}
	\end{center}
	\caption{Input parameters for the ring problem.}
	\label{tab:2}
\end{table}

\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{pics/RingPorous_3.png}
	\caption{Numerical results for the ring with a rolled porous medium inside.}
	\label{RingPorous}
\end{figure}

Fig.~\ref{RingPorous} (left) illustrates the computational domain discretized using the adaptive grid with resolution $120\times29$. The geometries in red and blue denote the porous and fluid regions, respectively. In the Fig.~\ref{RingPorous} (middle, right), the pressure and velocity profiles are shown. The computed pressure difference is $2061.2\;KPa$. This compares well to the analytically computed pressure difference, which is equal to $2067.1\;KPa$. Note that the analytical pressure difference in the fluid domain is neglected as it is very small compared to the pressure drop across the porous media.

\emph{Stokes-Brinkman flow in a channel with a single U-shaped pleat.}
The domain is a 2D channel of dimension of height 30mm and width 3mm with a U-shaped porous pleat of length 17mm and width 3mm. The inflow parameters for the flow problem are viscosity $\mu = 3.3 e^{-3}$, inlet velocity $u_{in} = 1.85 mm/s $, density $\rho=826 kg/m^3$, and permeability $K=2.5e^{-12}m^2$ .

Figure \ref{PleatQuadri} (left) illustrates a 2D computational grid comprising of quadrilaterals. The grid is generated using a quad morph forward marching algorithm \cite{QMorph}. The domain consists of  $5447$ quadrilaterals. The color red and blue denote the porous and fluid regions, respectively. Figure \ref{PleatQuadri} (middle, right) shows the velocity and pressure distrubtion in the domain. The computed pressure drop is 0.179981 kPa. 

The results on the complex quadrilateral grid using the above described algorithm are compared to the results of a commercial software tool called FiltEST.   
Figure \ref{PleatVoxels} (left) illustrates a cross section slice of the 3D Cartesian computational grid comprising of voxels. The grid is generated and the simulation results are obtained using the FiltEST software \cite{FiltEST}. The domain consists of  $60 \times 640 \times 60$ voxels. The small grid resolution of 0.05 mm is considered to overcome the stair case grid effects along the pleat curvature such that the 2D cross section consists of  . The color red and blue denote the porous and fluid regions, respectively. In Figure \ref{PleatVoxels} (middle, right) shows the velocity and pressure distrubtion in the domain. The computed pressure drop is 0.1911504 kPa. It can be observed that the results correlate well. 

\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{pics/Pleat_Quadri.png}
	\caption{Numerical results for the pleat on quadrilateral grid.}
	\label{PleatQuadri}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{pics/Pleat_Voxels.png}
	\caption{Numerical results for the pleat on voxel grid using FiltEST software.}
	\label{PleatVoxels}
\end{figure}