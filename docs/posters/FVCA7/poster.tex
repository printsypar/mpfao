\documentclass[portrait, ***30pt***]{a0poster}

% Packages
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage{amsmath,amssymb}
\usepackage{multicol}
\usepackage{subfigure}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{accents}
\usepackage{color}
\usepackage{tikz,lipsum}
\usepackage{helvet}
\usepackage[T1]{fontenc}
\usepackage{array}
\usepackage{natbib}
\usepackage{setspace}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{multirow}
\usepackage{hhline}

\graphicspath{./imgs/}
\renewcommand{\familydefault}{\sfdefault}
\newenvironment{block}[1]{\par \textcolor{BoxCol}{\fontsize{32pt}{22}\selectfont\bf #1}\\*}{\vskip 1.5cm}
\setlength{\parindent}{0pt}
 \renewcommand{\thefootnote}{\fnsymbol{footnote}}

\newcommand{\postertitle}{MPFA Algorithm for Solving Stokes-Brinkman Equations on Quadrilateral Grids}
\newcommand{\postersubtitle}{FVCA7 - The International Symposium of Finite Volumes for Complex Applications VII, Berlin, June 15-20, 2014}
\newcommand{\posterauthor}{
  Oleg Iliev\footnotemark[1]\footnotemark[2],
  Ralf Kirsch\footnotemark[1],
  Zahra Lakdawala\footnotemark[3],
  \underline{Galina Printsypar}\footnotemark[2]
}
\newcommand{\subblock}[1]{\textcolor{gray}{\fontsize{26pt}{20}\selectfont
\textbf{\textit{#1}}}}

% Commands
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
%\DeclareDocumentCommand \E {m O{}} {{ \mathop{\ensuremath{\mathrm{E}_{#2}}}\mspace{-2mu}\left[#1\right]}}
\providecommand{\E}[1]{{\ensuremath{\mathrm{E}}\mspace{-2mu}\left[#1\right]}}% Expected Value
\providecommand{\Esym}{{\mathrm{E}}}% Expected Value
\providecommand{\var}[1]{{\ensuremath{\mathrm{Var}}\mspace{-2mu}\left[#1\right]}}% Variance
%\providecommand{\vg}{{\ensuremath{\var{g_0}}}}
\providecommand{\vg}{{\ensuremath{V_0}}}
\providecommand{\tol}{\mathrm{TOL}}
\providecommand{\rset}{\mathbb{R}}
\providecommand{\zset}{\mathbb{Z}}
\providecommand{\nset}{\mathbb{N}}
\providecommand{\discreteh}{\mathfrak{H}}
%\providecommand{\We}{\ensuremath{\mathbf{W}}}
%\providecommand{\St}{\ensuremath{\mathbf{S}}}
%\providecommand{\Vz}{\ensuremath{\mathbf{V}}}
\providecommand{\hier}{\ensuremath{\mathbf{H}}}
\providecommand{\work}{\ensuremath{W}}
\providecommand{\biaserr}{\ensuremath{e_B}}
\providecommand{\staterr}{\ensuremath{e_S}}
\providecommand{\FuncName}[1]{\textsc{#1}}
\providecommand{\todo}[1]{\footnote{#1}}
% \providecommand{\overhat}{\widehat} % Tempory fix to make the code compile!
%\providecommand{\history}{\ensuremath{\mathscr{H}}} % requires package mathrsfs
\providecommand{\history}{\ensuremath{\mathcal{H}}}
\providecommand{\tuple}[1]{ \ensuremath{ \left \lbrace #1 \right \rbrace_{\ell=0}^L }}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\normal}{\mathbf{n}}
\newcommand{\inlet}{\text{in}}
\newcommand{\outlet}{\text{out}}
\newcommand{\pdrop}{p_\text{drop}}
\newcommand{\bdry}{\Gamma} %boundary
\newcommand{\visc}{\mu} % Dynamic viscosity
\newcommand{\dens}{\rho} % Density
\newcommand{\vel}{\mathbf{u}} % Fluid velocity
\newcommand{\fspeed}{u} % Flow speed
\newcommand{\force}{\mathbf{f}} % Force field
\newcommand{\perm}{\mathbf K} % permeability

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Layout parameters
\definecolor{BoxCol}{rgb}{0.86,0.375,0.128}
\definecolor{SubTitleColor}{RGB}{247,216,200}

\begin{document}
%%%%%%%%%%%%%%%%%% BEGIN: Style stuff
    \doublespacing
    \begin{tikzpicture}[remember picture,overlay]
        \draw[line width=5mm,BoxCol, rounded corners=10pt, fill]
        ([shift={(-2.5\pgflinewidth,-2.5\pgflinewidth)}]current page.north east)
        rectangle
        ([shift={(2.5\pgflinewidth,2.5\pgflinewidth)}]current page.south west);
        \draw[line width=5mm,white, fill]
        ([shift={(-5.0\pgflinewidth,-20.0\pgflinewidth)}]current page.north east)
        rectangle
        ([shift={(5.0\pgflinewidth,10.0\pgflinewidth)}]current page.south west);
    \end{tikzpicture}
    \begin{tabular}{ m{0.75\textwidth} m{0.25\textwidth}}
        \bf \fontsize{68}{45}\selectfont
        \hskip 0cm \textcolor{white}{\postertitle} \\
	\vskip 0cm \textbf{\textit{ \fontsize{34}{5}\selectfont \textcolor{SubTitleColor}{\posterauthor}}}
	&
        \vskip -5.9cm \hskip 2cm \includegraphics[scale=1.3]{imgs/LOGO_KAUST4_white.png}
    \end{tabular}
    \
    \vskip 3cm \hskip 2cm
    \begin{minipage}[t][0.905\textheight][t]{0.88\textwidth}
     \setlength{\columnsep}{3cm}
    \begin{multicols}{2}

    \textbf{\textcolor{gray}{\noindent
        \footnotemark[1] Fraunhofer Institute for Industrial Mathematics, Kaiserslautern, Germany
        \\ \footnotemark[2] NumPor, King Abdullah University of Science and Technology, Saudi Arabia
        \\ \footnotemark[3] DHI-WASY GmbH, Berlin, Germany}}
    \vskip 3cm
%%%%%%%%%%%%%%% END: Style stuff

\begin{block}{Introduction}
Recently, different discretization techniques, such as the multi point
flux approximation (MPFA) and mimetic finite differences (MFD), have
been developed to be able to accurately solve PDEs, mainly in oil recovery and hydrology applications with highly varying coefficients on general rough meshes.
%\\
\vskip 0.2cm
\centerline{\includegraphics[width=0.42\textwidth]{imgs/filterElement_cropped.pdf}}

Here, we are concerned with filtration problems where one of the
challenges is the complicated shape of the computational domain
\cite{laptev}. Therefore, filtration problems requires sophisticated discretization and numerical algorithms. We will focus on a numerical algorithm which adapts the MPFA O-method (see e.g. \cite{aavatsmark}).
\end{block}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{Modeling of Stokes-Brinkman equations}
To model filtration problems we use the Stokes-Brinkman equations, which read
\begin{equation}
	\label{eq:SB}
	\dens\dfrac{\partial\vel}{\partial t} - \nabla\cdot (\visc\nabla\vel)
	+ \visc\perm^{-1}\vel= -\nabla p,\qquad
	\nabla\cdot\vel = 0\,,\quad\mathbf x\in\Omega;
\end{equation}
\begin{equation}
	\label{sms:eq_BC_Vel_NSB_inlet}
	\vel(\mathbf x)=\vel_{\text{in}}(\mathbf x) \text{ on }\bdry_i, \ \ \ \ \
	\mathbf{\sigma}\cdot\mathbf{n}=0\text{ on }\bdry_o, \ \ \ \ \
	\vel(\mathbf x)=0\text{ on }\bdry_s;
\end{equation}
%
Here, $\vel$ and $p$  denote the fluid velocity vector and the fluid
pressure, respectively, $\visc$ is the fluid dynamic viscosity.
$\Omega$ is the computational domain, which consists of two
nonintersecting subsets, namely the fluid domain $\Omega_f$ and the
porous domain $\Omega_p$. $\perm$ is the intrinsic permeability of the
porous medium in $\Omega_p$ and $\perm^{-1}=0$ in $\Omega_f$. Equations \eqref{sms:eq_BC_Vel_NSB_inlet} is a typical set of boundary conditions.

The finite volume integral formulation of Chorin type method for equations \eqref{eq:SB} yields
\begin{align}
	\label{Chorin-ustar}
	\int_{v}\dfrac{\rho}{\tau}\big(\vel^\ast-\vel^{n}\big) d\mathbf{x}
	{\color{red}- \int_{v}\nabla \cdot \mu \nabla\vel^\ast d\mathbf{x}} + \int_{v} \mu \perm^{-1} \vel^\ast d\mathbf{x}
	= {\color{blue}-\int_{v} \nabla p^{n} d\mathbf{x}},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-pc}
	{\color{red}- \int_{v} \nabla \cdot \left( \left(\dfrac{\rho}{\tau}
	+ \mu \perm^{-1} \right)^{-1} \nabla p^{'}\right) d\mathbf{x}}
	= -\int_v \nabla \cdot \vel^\ast d\mathbf{x},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-velupdate1}
	\vel^{n+1}_v= \frac{1}{V}\int_{v} \left( \vel^\ast_v{\color{blue}-\left(\dfrac{\rho}{\tau}
	+\mu \perm^{-1}\right)^{-1} \nabla p^{'}}\right) d\mathbf{x},
	&\quad v\in\mathcal{V};
	\\
	\label{Chorin-prupdate}
	p^{n+1}_v = p^{n}_v + p^{'}_v,
	&\quad v\in\mathcal{V};
\end{align}
where index '$v$' denotes volume averaged variables, $p'$ is the pressure correction, $\mathcal{V}$ is a set of quadrilateral finite volumes $v$ subdividing the computational domain $\Omega$, $V$ is the measure of the finite volume $v$, $V=mes(v)$.
\end{block}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{Discretization}
\label{sec:mpfa}
\begin{minipage}[t]{0.2\textwidth}
	\vspace{-4.5cm}
	The Multipoint Flux Approximation method (MPFA) is employed to
        approximate two types of terms: $\int_{v} \nabla \cdot
        \alpha \nabla \phi d\mathbf{x}$ and $\int_{v} \nabla \phi
        d\mathbf{x}$, where $\phi$ is a scalar function representing
        pressure or one of the velocity components.
\vspace{1cm}
\end{minipage}
\begin{minipage}{0.3\textwidth}
	\begin{center}
	\hspace{-2cm}
	\includegraphics[width=0.82\textwidth]{imgs/grid.png}
	\end{center}
\end{minipage}
%
\subblock{Approximation of {\color{red}$-\int_{v} \nabla \cdot \alpha \nabla \phi d\mathbf{x}$.}}
%
The evaluation of the flux is reduced to calculation of the following integral, which is standard for the MPFA method
\begin{equation}
	\label{eq:flux}
	f_v = -\int_{v} \nabla \cdot \alpha \nabla \phi d\mathbf{x}
	= -\oint_{\partial v} (\alpha \nabla \phi) \cdot \normal \, dl
	= \sum_{i=0}^{n_e-1}f_i
	\approx \sum_{i=0}^{n_e-1}\left(\sum_{j=0}^{n_p-1}t_{ij}\hat{\phi}_j\right).
\end{equation}
	$f_i$ is the flux through half edges $e_i$
	with $n_e=8$ denoting the number of half edges of the primary cell and
	$n_p=4$ denoting the number of primary quadrilaterals in the dual cell,
	$t_{ij}$ is called the transmissibility coefficient,
	computed via the MPFA O-method
	$\hat{\phi}_j$ is the value of function $\phi$ at the center
        of the $j$'th primary quadrilateral in the dual cell.

%
\subblock{Approximation of {\color{blue}$\int_{v} \nabla \phi d\mathbf{x}$.}}
Here, the MPFA method is extended in order to approximate the gradient operators
\begin{equation}
	\label{Green}
	\int_{v} \frac{\partial \phi}{\partial x} d\mathbf{x}
	= \oint_{\partial v} 0dx + \phi dy
	= \oint_{\partial v} (\phi, 0)\cdot (dy, -dx)
	= \oint_{\partial v} (\phi, 0)\cdot \mathbf{n} dl \\
	= \oint_{\partial v} \phi n_x dl
	\approx \sum_{i=0}^{n_e-1}\phi_i s_i n_{x,i}.
\end{equation}
where
	$\phi_i$ are the values of $\phi$ on $e_i$,
	$s_i$ is the measure of $e_i$, $s_i=mes(e_i)$,
	$\mathbf{n}_i=(n_{x,i},n_{y,i})$ is the outward unit normal vector of $e_i$.
Values of $\phi_i$ can be estimated using MPFA O-method.
\end{block}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{Numerical Experiments}
\subblock{Stationary channel flow between two parallel plates}

The analytical solution of this problem gives the maximum centerline velocity $u_{2,max} =  0.0225\;m/s$ and the pressure drop $\pdrop = 0.18\;Pa$. It can be seen that, despite the complexity of grids, results have a comparable pressure and velocity accuracy.
\begin{center}
\includegraphics[width=0.35\textwidth]{imgs/Grids_Channel.png}
\end{center}

\begin{minipage}{0.3\textwidth}
	\includegraphics[width=1\textwidth]{imgs/Velocities_Channel_2.png}
\end{minipage}
\begin{minipage}{0.19\textwidth}
\fontsize{20pt}{25}\selectfont
	\begin{tabular}[width=1\textwidth]{|l|l|l|l|}
	\hline
	\bf {Geometry} & \bf{Resolution} & $p_{drop}$, $[Pa]$ & $u_{2,max}$, $[m/s]$\\
	\hline
	\multirow{2}{*}{Cartesian} & $80 \times 40$ & $0,1749$ & $0,0228$\\
	\hhline{~---}
	& $40 \times 20$ & $0,19$ & $0,02269$ \\
	\hline
	\multirow{3}{*}{fish-bone} & $80 \times 40$ & $0,1745$ & $0,0228$ \\
	\hhline{~---}
	& $80 \times 20$ & $0,1859$ & $0,02214$ \\
	\hhline{~---}
	& $40 \times 20$ & $0,185$ & $0,02268$ \\
	\hline
	\multirow{3}{*}{mosaic} & $80 \times 40$ & $0,1735$ & $0,02268$ \\
	\hhline{~---}
	& $80 \times 20$ & $0,1829$ & $0,02192$ \\
	\hhline{~---}
	& $40 \times 20$ & $0,1882$ & $0,02247$ \\
	\hline
	\end{tabular}
\end{minipage}
\vspace{1cm}

\subblock{Radial Stokes-Brinkman flow in a ring}

The figure below illustrates the computational domain
when discretized using the quadrilateral grid with resolution $120\times29$. The computed pressure difference is $2061.2\;KPa$. This compares well to the analytically computed pressure difference, which is equal to $2067.1\;KPa$.
\begin{center}
\includegraphics[width=0.45\textwidth]{imgs/RingPorous_3.png}
\end{center}

\subblock{Stokes-Brinkman flow in a section with a single U-shaped pleat}

The results for a two dimensional section of a pleated filter with height $30\,mm$ and
width $3\,mm$ which discretized with $5447$ quadrilaterals, presented below.
\vskip 1cm
\begin{center}
\includegraphics[width=0.48\textwidth]{imgs/pleat_cropped.pdf}
\end{center}

\end{block}

\begin{block}{References}
        \singlespacing
        \vspace{-2cm}
        \renewcommand{\section}[2]{}%
        \bibliographystyle{abbrv}
        {\footnotesize \bibliography{ref}}
\end{block}

    \end{multicols}
 \end{minipage}

  \vskip -0.5cm
    \textcolor{SubTitleColor}{\fontsize{32pt}{1}\selectfont
	\postersubtitle
    %Acknowledgements: This work is a collaboration with
    }
\end{document}
