#include"errorhandler.h"
#include<stdio.h>
#include<stdlib.h>
//-----------------------------------------------------------------------------
void ERRORHANDLER::Exit(const char *errorDescription)
{
  fprintf(stderr,"%s\n",errorDescription);
  FILE *F;
  if ((F = fopen("error.txt","w")) == NULL) {
    fprintf(stderr, "ERRORHANDLER::Exit: Can't open file\n");
    exit(1);
  }
  fprintf(F,"%s\n",errorDescription);
  if (ferror(F) != 0) {
    fprintf(stderr, "ERRORHANDLER::Exit: Can't write file\n");
    exit(1);
  }
  if (fclose(F) != 0) {
    fprintf(stderr, "ERRORHANDLER::Exit: Can't close file\n");
    exit(1);
  }
  exit(1);
}
//-----------------------------------------------------------------------------
