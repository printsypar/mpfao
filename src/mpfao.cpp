#include "mpfao.h"
mpfao::mpfao()
{
    coord = new MATRIX();
    perm = new MATRIX();
    edgeType = NULL;
    G = new MATRIX();
    ph = new MATRIX();
    flux = new MATRIX();
    n = new MATRIX();
    Nu = new MATRIX();
    delta = NULL;
    sigma = NULL;
    alpha = NULL;
    beta = NULL;
    b = NULL;
    c = NULL;
}
mpfao::~mpfao()
{
    delete coord;
    delete perm;
    if (edgeType != NULL) delete[] edgeType;
    delete G;
    delete ph;
    delete flux;
    delete n;
    delete Nu;
    if (delta != NULL) delete[] delta;
    if (sigma != NULL) delete[] sigma;
    if (alpha != NULL) {
        for (int i=0; i<N; i++)
            delete [] alpha[i];
        delete [] alpha;
    }
    if (beta != NULL) {
        for (int i=0; i<N; i++)
            delete [] beta[i];
        delete [] beta;
    }
    if (b != NULL) delete []b;
    if (c != NULL) delete []c;
}
void mpfao::init(MATRIX &_coord, MATRIX &_perm, int _edgeType[])
{
    N = (int)(_coord.GetNEl(XDIR)-1)/2;
    *coord = _coord;
    if (_perm.GetNEl(XDIR)!=N && _perm.GetNEl(XDIR)!=2*N)
        ERRORHANDLER::Instance().Exit("mpfao::wrong size of the permeability matrix");
    *perm = _perm;
    if (edgeType!=NULL) delete []edgeType;
    edgeType = new int[N];
    if (b!=NULL) delete []b;
    b = new double[N];
    if (c!=NULL) delete []c;
    c = new double[N];
    for (int i=0; i<N; i++) {
        edgeType[i]=_edgeType[i];
        b[i] = 0.0;
        c[i] = 0.0;
    }

    G->SetNEl(2*N,2,1);
    ph->SetNEl(N,N+1,1);
    flux->SetNEl(N,N+1,1);
    n->SetNEl(N,2,1);
    Nu->SetNEl(2*N,2,1);
    if(delta!=NULL) delete []delta;
    delta = new double[N];
    if(sigma!=NULL) delete []sigma;
    sigma = new double[N];
}
void mpfao::init(MATRIX &_coord, MATRIX &_perm, int _edgeType[], double *_c)
{
    N = (int)(_coord.GetNEl(XDIR)-1)/2;
    *coord = _coord;
    if (_perm.GetNEl(XDIR)!=N && _perm.GetNEl(XDIR)!=2*N)
        ERRORHANDLER::Instance().Exit("mpfao::wrong size of the permeability matrix");
    *perm = _perm;
    if (edgeType!=NULL) delete []edgeType;
    edgeType = new int[N];
    if (b!=NULL) delete []b;
    b = new double[N];
    if (c!=NULL) delete []c;
    c = new double[N];
    for (int i=0; i<N; i++) {
        edgeType[i]=_edgeType[i];
        b[i] = 0.0;
        c[i] = _c[i];
    }

    G->SetNEl(2*N,2,1);
    ph->SetNEl(N,N+1,1);
    flux->SetNEl(N,N+1,1);
    n->SetNEl(N,2,1);
    Nu->SetNEl(2*N,2,1);
    if(delta!=NULL) delete []delta;
    delta = new double[N];
    if(sigma!=NULL) delete []sigma;
    sigma = new double[N];
}
void mpfao::init(MATRIX &_coord, MATRIX &_perm, int _edgeType[], double *_b, double *_c)
{
    N = (int)(_coord.GetNEl(XDIR)-1)/2;
    *coord = _coord;
    if (_perm.GetNEl(XDIR)!=N && _perm.GetNEl(XDIR)!=2*N)
        ERRORHANDLER::Instance().Exit("mpfao::wrong size of the permeability matrix");
    *perm = _perm;
    if (edgeType!=NULL) delete []edgeType;
    edgeType = new int[N];
    if (b!=NULL) delete []b;
    b = new double[N];
    if (c!=NULL) delete []c;
    c = new double[N];
    for (int i=0; i<N; i++) {
        edgeType[i]=_edgeType[i];
        b[i] = _b[i];
        c[i] = _c[i];
    }

    G->SetNEl(2*N,2,1);
    ph->SetNEl(N,N+1,1);
    flux->SetNEl(N,N+1,1);
    n->SetNEl(N,2,1);
    Nu->SetNEl(2*N,2,1);
    if(delta!=NULL) delete []delta;
    delta = new double[N];
    if(sigma!=NULL) delete []sigma;
    sigma = new double[N];
}
void mpfao::init(int _N, double x[], double y[], double **_perm, int _edgeType[], double _c[])
{
    N = _N;
    delete coord;
    coord = new MATRIX();
    coord->SetNEl(2*N+1,2,1);
    for (int i=0; i<2*N+1; i++) {
        coord->SetData(i,0,0,x[i]);
        coord->SetData(i,1,0,y[i]);
    }
    delete perm;
    perm = new MATRIX();
    perm->SetNEl(N,2,2);
    for(int i=0; i<N; i++) {
        perm->SetData(i,0,0,_perm[i][0]);
        perm->SetData(i,0,1,_perm[i][1]);
        perm->SetData(i,1,0,_perm[i][2]);
        perm->SetData(i,1,1,_perm[i][3]);
    }
    if (edgeType!=NULL) delete []edgeType;
    edgeType = new int[N];
    if (b!=NULL) delete []b;
    b = new double[N];
    if (c!=NULL) delete []c;
    c = new double[N];
    for (int i=0; i<N; i++) {
        edgeType[i]=_edgeType[i];
        b[i] = 0.0;
        c[i] = _c[i];
    }

    G->SetNEl(2*N,2,1);
    ph->SetNEl(N,N+1,1);
    flux->SetNEl(N,N+1,1);
    n->SetNEl(N,2,1);
    Nu->SetNEl(2*N,2,1);
    if(delta!=NULL) delete []delta;
    delta = new double[N];
    if(sigma!=NULL) delete []sigma;
    sigma = new double[N];
}
void mpfao::runMPFAComputations(void)
{
    bool inner=true;
    for(int i=0; i<N; i++)
        if(edgeType[i]!=1) inner=false;

    if(inner){
        setInnerCVAuxVariables();
        setInnerCVG();
        setInnerCVPh();
        setInnerCVFlux(G, flux);
    } else {
        setBoundaryCVAuxVariables();
        setBoundaryCVG();
        setBC();
        setBoundaryCVPh();
        setBoundaryCVFlux(G, flux);
    }
}
void mpfao::runMPFAComputationsWithoutBC(void)
{
    bool inner=true;
    for(int i=0; i<N; i++)
        if(edgeType[i]!=1) inner=false;

    if(inner){
        setInnerCVAuxVariables();
        setInnerCVG();
        setInnerCVPh();
        setInnerCVFlux(G, flux);
        this->setNEl(N,N);
    } else {
        setBoundaryCVAuxVariables();
        setBoundaryCVG();
        setBC();
        setBoundaryCVPhWithoutBC();
        setBoundaryCVFluxWithoutBC(G, flux);
    }
}
MATRIX mpfao::projectFluxToVector(double nx, double ny)
{
    MATRIX *auxG = new MATRIX();
    MATRIX projFlux;

    bool inner=true;
    for(int i=0; i<N; i++)
        if(edgeType[i]!=1) inner=false;
    if(inner){
        setInnerCVG(nx, ny, auxG);
        setInnerCVFlux(auxG, &projFlux);
    } else {
        setBoundaryCVG(nx, ny, auxG);
        setBoundaryCVFlux(auxG, &projFlux);
    }

    delete auxG;
    return projFlux;
}
MATRIX mpfao::projectFluxToVectorWithoutBC(double nx, double ny)
{
    MATRIX *auxG = new MATRIX();
    MATRIX projFlux;

    bool inner=true;
    for(int i=0; i<N; i++)
        if(edgeType[i]!=1) inner=false;
    if(inner){
        setInnerCVG(nx, ny, auxG);
        setInnerCVFlux(auxG, &projFlux);
        this->setNEl(N,N);
    } else {
        setBoundaryCVG(nx, ny, auxG);
        setBoundaryCVFluxWithoutBC(auxG, &projFlux);
    }
    delete auxG;
    return projFlux;
}
void mpfao::setNormal(double nx, double ny)
{
    for(int i=0; i<N; i++) {
        n->SetData(i,0,0,nx);
        n->SetData(i,1,0,ny);
    }
}
void mpfao::updateBC(int _edgeType[], double *_c)
{
    for (int i=0; i<N; i++) {
        edgeType[i]=_edgeType[i];
        b[i] = 0.0;
        c[i] = _c[i];
    }
}
void mpfao::setInnerCVAuxVariables(void)
{
    int i1, i2, i3;
    double x[3], y[3];
    double X, Y;
    for(int i=0; i<N; i++) {
        if (i==0) i1 = 2*N;
        else i1 = 2*i;
        i2 = 2*i+1;
        i3 = 2*i+2;
        x[0] = coord->GetData(i1,0,0);
        y[0] = coord->GetData(i1,1,0);
        x[1] = coord->GetData(i2,0,0);
        y[1] = coord->GetData(i2,1,0);
        x[2] = coord->GetData(i3,0,0);
        y[2] = coord->GetData(i3,1,0);
        delta[i] = 0.5*fabs((x[1]-x[2])*(y[0]-y[2])-(x[0]-x[2])*(y[1]-y[2]));

        i1 = 2*i;
        if (i==0) i2 = 2*N-1;
        else i2 = 2*i-1;
        normal(x[1],y[1],x[2],y[2],&X,&Y);
        outwardVector(x[1],y[1],x[2],y[2],coord->GetData(0,0,0),coord->GetData(0,1,0),&X,&Y);
        Nu->SetData(i1,0,0,X);
        Nu->SetData(i1,1,0,Y);
        normal(x[0],y[0],x[1],y[1],&X,&Y);
        outwardVector(x[0],y[0],x[1],y[1],coord->GetData(0,0,0),coord->GetData(0,1,0),&X,&Y);
        Nu->SetData(i2,0,0,X);
        Nu->SetData(i2,1,0,Y);
    }
    x[0] = coord->GetData(0,0,0);
    y[0] = coord->GetData(0,1,0);
    for(int i=0; i<N; i++) {
        x[1] = coord->GetData(2*i+2,0,0);
        y[1] = coord->GetData(2*i+2,1,0);
        sigma[i] = pow(pow(x[1]-x[0],2.)+pow(y[1]-y[0],2.),0.5);
        unitNormal(x[1], y[1], x[0], y[0], &X, &Y);
        outwardVector(x[1],y[1],x[0],y[0],coord->GetData(2*i+1,0,0),coord->GetData(2*i+1,1,0),&X,&Y);
        n->SetData(i,0,0,X);
        n->SetData(i,1,0,Y);
    }
}
void mpfao::setInnerCVG(void)
{
    double temp1, temp2;
    int i1;
    for(int i=0; i<N; i++) {
        if(perm->GetNEl(XDIR)==N) {
            temp1 = n->GetData(i,0,0)*perm->GetData(i,0,0)+n->GetData(i,1,0)*perm->GetData(i,1,0);
            temp2 = n->GetData(i,0,0)*perm->GetData(i,0,1)+n->GetData(i,1,0)*perm->GetData(i,1,1);
        } else {
            temp1 = n->GetData(i,0,0)*perm->GetData(2*i,0,0)+n->GetData(i,1,0)*perm->GetData(2*i,1,0);
            temp2 = n->GetData(i,0,0)*perm->GetData(2*i,0,1)+n->GetData(i,1,0)*perm->GetData(2*i,1,1);
        }

        if (i==0) i1=2*N-1;
        else i1=2*i-1;
        G->SetData(2*i,0,0,-sigma[i]/2./delta[i]*(-temp1*Nu->GetData(2*i,0,0)-temp2*Nu->GetData(2*i,1,0)));
        G->SetData(2*i,1,0,-sigma[i]/2./delta[i]*(temp1*Nu->GetData(i1,0,0)+temp2*Nu->GetData(i1,1,0)));

        if(i==N-1)
            i1=0;
        else i1=i+1;
        if(perm->GetNEl(XDIR)==N) {
            temp1 = n->GetData(i,0,0)*perm->GetData(i1,0,0)+n->GetData(i,1,0)*perm->GetData(i1,1,0);
            temp2 = n->GetData(i,0,0)*perm->GetData(i1,0,1)+n->GetData(i,1,0)*perm->GetData(i1,1,1);
        } else {
            temp1 = n->GetData(i,0,0)*perm->GetData(2*i+1,0,0)+n->GetData(i,1,0)*perm->GetData(2*i+1,1,0);
            temp2 = n->GetData(i,0,0)*perm->GetData(2*i+1,0,1)+n->GetData(i,1,0)*perm->GetData(2*i+1,1,1);
        }
        G->SetData(2*i+1,0,0,-sigma[i]/2./delta[i1]*(-temp1*Nu->GetData(2*i1,0,0)-temp2*Nu->GetData(2*i1,1,0)));
        G->SetData(2*i+1,1,0,-sigma[i]/2./delta[i1]*(temp1*Nu->GetData(2*i+1,0,0)+temp2*Nu->GetData(2*i+1,1,0)));
    }
}
void mpfao::setInnerCVG(double nx, double ny, MATRIX* auxG)
{
    auxG->SetNEl(2*N,2,1);
    double temp1, temp2;
    int i1;
    for(int i=0; i<N; i++) {
        if(perm->GetNEl(XDIR)==N) {
            temp1 = nx*perm->GetData(i,0,0)+ny*perm->GetData(i,1,0);
            temp2 = nx*perm->GetData(i,0,1)+ny*perm->GetData(i,1,1);
        } else {
            temp1 = nx*perm->GetData(2*i,0,0)+ny*perm->GetData(2*i,1,0);
            temp2 = nx*perm->GetData(2*i,0,1)+ny*perm->GetData(2*i,1,1);
        }

        if (i==0) i1=2*N-1;
        else i1=2*i-1;
        auxG->SetData(2*i,0,0,-sigma[i]/2./delta[i]*(-temp1*Nu->GetData(2*i,0,0)-temp2*Nu->GetData(2*i,1,0)));
        auxG->SetData(2*i,1,0,-sigma[i]/2./delta[i]*(temp1*Nu->GetData(i1,0,0)+temp2*Nu->GetData(i1,1,0)));

        if(i==N-1)
            i1=0;
        else i1=i+1;
        if(perm->GetNEl(XDIR)==N) {
            temp1 = nx*perm->GetData(i1,0,0)+ny*perm->GetData(i1,1,0);
            temp2 = nx*perm->GetData(i1,0,1)+ny*perm->GetData(i1,1,1);
        } else {
            temp1 = nx*perm->GetData(2*i+1,0,0)+ny*perm->GetData(2*i+1,1,0);
            temp2 = nx*perm->GetData(2*i+1,0,1)+ny*perm->GetData(2*i+1,1,1);
        }
        auxG->SetData(2*i+1,0,0,-sigma[i]/2./delta[i1]*(-temp1*Nu->GetData(2*i1,0,0)-temp2*Nu->GetData(2*i1,1,0)));
        auxG->SetData(2*i+1,1,0,-sigma[i]/2./delta[i1]*(temp1*Nu->GetData(2*i+1,0,0)+temp2*Nu->GetData(2*i+1,1,0)));
    }
}
void mpfao::setInnerCVPh(void)
{
    MATRIX A, B, C;
    A.SetNEl(N,N,1);
    B.SetNEl(N+1,N,1);
    C.SetNEl(N+1,N,1);
    A.SetData(0.0);
    B.SetData(0.0);
    int i1, i2;
    for (int i=0; i<N; i++) {
        if (i==0) i1=N-1;
        else i1=i-1;
        if (i==N-1) i2=0;
        else i2=i+1;
        A.SetData(i1,	i,	0,	G->GetData(2*i,0,0));
        A.SetData(i,	i,	0,	-G->GetData(2*i,1,0)-G->GetData(2*i+1,0,0));
        A.SetData(i2,	i,	0,	G->GetData(2*i+1,1,0));
        B.SetData(i, 	i, 	0, 	G->GetData(2*i,0,0)-G->GetData(2*i,1,0));
        B.SetData(i2,	i,	0, 	-G->GetData(2*i+1,0,0)+G->GetData(2*i+1,1,0));
    }
    A = A.Inverse2D();
    C = A*B;
    for(int i=0; i<N; i++)
        for(int j=0; j<N+1; j++)
            ph->SetData(i,j,0,C.GetData(j,i,0));
}
void mpfao::setInnerCVFlux(MATRIX* auxG, MATRIX* auxFlux)
{
    auxFlux->SetNEl(N,N+1,1);
    MATRIX A, B, C;
    A.SetNEl(N+1,N,1);
    B.SetNEl(N,N,1);
    C.SetNEl(N+1,N,1);
    A.SetData(0.0);
    B.SetData(0.0);
    C.SetData(0.0);
    int i1;
    for (int i=0; i<N; i++) {
        if (i==0) i1=N-1;
        else i1=i-1;
        A.SetData(i,	i,	0,	-auxG->GetData(2*i,0,0)+auxG->GetData(2*i,1,0));
        B.SetData(i1,	i,	0, 	 auxG->GetData(2*i,0,0));
        B.SetData(i,	i,	0, 	-auxG->GetData(2*i,1,0));
        for (int j=0; j<N; j++)
            C.SetData(j,i,0,ph->GetData(i,j,0));
    }
    C = A+B*C;
    for(int i=0; i<N; i++)
        for(int j=0; j<N+1; j++)
            auxFlux->SetData(i,j,0,C.GetData(j,i,0));
}
void mpfao::setBoundaryCVAuxVariables(void)
{
    int i1, i2, i3;
    double x[3], y[3];
    double X, Y;
    for (int i=0; i<N; i++) {
        delta[i]=0.0;
        sigma[i]=0.0;
    }
    Nu->SetData(0.0);
    n->SetData(0.0);
    for(int i=0; i<N; i++) {
        if (i==0) i1=N-1;
        else i1=i-1;
        if(edgeType[i]>0 && edgeType[i1]>0) {
            if (i==0) i1 = 2*N;
            else i1 = 2*i;
            i2 = 2*i+1;
            i3 = 2*i+2;
            x[0] = coord->GetData(i1,0,0);
            y[0] = coord->GetData(i1,1,0);
            x[1] = coord->GetData(i2,0,0);
            y[1] = coord->GetData(i2,1,0);
            x[2] = coord->GetData(i3,0,0);
            y[2] = coord->GetData(i3,1,0);
            delta[i] = 0.5*fabs((x[1]-x[2])*(y[0]-y[2])-(x[0]-x[2])*(y[1]-y[2]));

            i1 = 2*i;
            if (i==0) i2 = 2*N-1;
            else i2 = 2*i-1;
            normal(x[1],y[1],x[2],y[2],&X,&Y);
            outwardVector(x[1],y[1],x[2],y[2],coord->GetData(0,0,0),coord->GetData(0,1,0),&X,&Y);
            Nu->SetData(i1,0,0,X);
            Nu->SetData(i1,1,0,Y);
            normal(x[0],y[0],x[1],y[1],&X,&Y);
            outwardVector(x[0],y[0],x[1],y[1],coord->GetData(0,0,0),coord->GetData(0,1,0),&X,&Y);
            Nu->SetData(i2,0,0,X);
            Nu->SetData(i2,1,0,Y);
        }
    }
    x[0] = coord->GetData(0,0,0);
    y[0] = coord->GetData(0,1,0);
    for(int i=0; i<N; i++) {
        if(edgeType[i]>0) {
            x[1] = coord->GetData(2*i+2,0,0);
            y[1] = coord->GetData(2*i+2,1,0);
            sigma[i] = pow(pow(x[1]-x[0],2.)+pow(y[1]-y[0],2.),0.5);
            unitNormal(x[1], y[1], x[0], y[0], &X, &Y);
            if (i==0) i1=N-1;
            else i1=i-1;
            if(edgeType[i1]>0)
                outwardVector(x[1],y[1],x[0],y[0],coord->GetData(2*i+1,0,0),coord->GetData(2*i+1,1,0),&X,&Y);
            else {
                if (i==N-1) i1=1;
                else i1=2*i+3;
                inwardVector(x[1],y[1],x[0],y[0],coord->GetData(i1,0,0),coord->GetData(i1,1,0),&X,&Y);
            }
            n->SetData(i,0,0,X);
            n->SetData(i,1,0,Y);
        }
    }
}
void mpfao::setBoundaryCVG(void)
{
    double temp1, temp2;
    int i1;
    G->SetData(0.0);
    for(int i=0; i<N; i++) {
        if(i==0) i1=N-1;
        else i1=i-1;
        if(edgeType[i]>0 && edgeType[i1]>0) {
            if(perm->GetNEl(XDIR)==N) {
                temp1 = n->GetData(i,0,0)*perm->GetData(i,0,0)+n->GetData(i,1,0)*perm->GetData(i,1,0);
                temp2 = n->GetData(i,0,0)*perm->GetData(i,0,1)+n->GetData(i,1,0)*perm->GetData(i,1,1);
            } else {
                temp1 = n->GetData(i,0,0)*perm->GetData(2*i,0,0)+n->GetData(i,1,0)*perm->GetData(2*i,1,0);
                temp2 = n->GetData(i,0,0)*perm->GetData(2*i,0,1)+n->GetData(i,1,0)*perm->GetData(2*i,1,1);
            }

            if (i==0) i1=2*N-1;
            else i1=2*i-1;
            G->SetData(2*i,0,0,-sigma[i]/2./delta[i]*(-temp1*Nu->GetData(2*i,0,0)-temp2*Nu->GetData(2*i,1,0)));
            G->SetData(2*i,1,0,-sigma[i]/2./delta[i]*(temp1*Nu->GetData(i1,0,0)+temp2*Nu->GetData(i1,1,0)));
        }

        if(i==N-1) i1=0;
        else i1=i+1;
        if(edgeType[i]>0 && edgeType[i1]>0) {
            if(i==N-1)
                i1=0;
            else i1=i+1;
            if(perm->GetNEl(XDIR)==N) {
                temp1 = n->GetData(i,0,0)*perm->GetData(i1,0,0)+n->GetData(i,1,0)*perm->GetData(i1,1,0);
                temp2 = n->GetData(i,0,0)*perm->GetData(i1,0,1)+n->GetData(i,1,0)*perm->GetData(i1,1,1);
            } else {
                temp1 = n->GetData(i,0,0)*perm->GetData(2*i+1,0,0)+n->GetData(i,1,0)*perm->GetData(2*i+1,1,0);
                temp2 = n->GetData(i,0,0)*perm->GetData(2*i+1,0,1)+n->GetData(i,1,0)*perm->GetData(2*i+1,1,1);
            }
            G->SetData(2*i+1,0,0,-sigma[i]/2./delta[i1]*(-temp1*Nu->GetData(2*i1,0,0)-temp2*Nu->GetData(2*i1,1,0)));
            G->SetData(2*i+1,1,0,-sigma[i]/2./delta[i1]*(temp1*Nu->GetData(2*i+1,0,0)+temp2*Nu->GetData(2*i+1,1,0)));
        }
    }
}
void mpfao::setBoundaryCVG(double nx, double ny, MATRIX* auxG)
{
    auxG->SetNEl(2*N,2,1);
    auxG->SetData(0.0);
    double temp1, temp2;
    int i1;
    for(int i=0; i<N; i++) {
        if(i==0) i1=N-1;
        else i1=i-1;
        if(edgeType[i]>0 && edgeType[i1]>0) {
            if(perm->GetNEl(XDIR)==N) {
                temp1 = nx*perm->GetData(i,0,0)+ny*perm->GetData(i,1,0);
                temp2 = nx*perm->GetData(i,0,1)+ny*perm->GetData(i,1,1);
            } else {
                temp1 = nx*perm->GetData(2*i,0,0)+ny*perm->GetData(2*i,1,0);
                temp2 = nx*perm->GetData(2*i,0,1)+ny*perm->GetData(2*i,1,1);
            }

            if (i==0) i1=2*N-1;
            else i1=2*i-1;
            auxG->SetData(2*i,0,0,-sigma[i]/2./delta[i]*(-temp1*Nu->GetData(2*i,0,0)-temp2*Nu->GetData(2*i,1,0)));
            auxG->SetData(2*i,1,0,-sigma[i]/2./delta[i]*(temp1*Nu->GetData(i1,0,0)+temp2*Nu->GetData(i1,1,0)));
        }
        if(i==N-1) i1=0;
        else i1=i+1;
        if(edgeType[i]>0 && edgeType[i1]>0) {
            if(i==N-1)
                i1=0;
            else i1=i+1;
            if(perm->GetNEl(XDIR)==N) {
                temp1 = nx*perm->GetData(i1,0,0)+ny*perm->GetData(i1,1,0);
                temp2 = nx*perm->GetData(i1,0,1)+ny*perm->GetData(i1,1,1);
            } else {
                temp1 = nx*perm->GetData(2*i+1,0,0)+ny*perm->GetData(2*i+1,1,0);
                temp2 = nx*perm->GetData(2*i+1,0,1)+ny*perm->GetData(2*i+1,1,1);
            }
            auxG->SetData(2*i+1,0,0,-sigma[i]/2./delta[i1]*(-temp1*Nu->GetData(2*i1,0,0)-temp2*Nu->GetData(2*i1,1,0)));
            auxG->SetData(2*i+1,1,0,-sigma[i]/2./delta[i1]*(temp1*Nu->GetData(2*i+1,0,0)+temp2*Nu->GetData(2*i+1,1,0)));
        }
    }
}
void mpfao::setBC(void)
{
    int i1, i2;
    alpha = new int*[N];
    beta = new int*[N];
    for (int i=0; i<N; i++) {
        alpha[i] = new int[7];
        beta[i] = new int[4];
        for (int j=0; j<7; j++) {
            alpha[i][j]=0;
            if (j<4) beta[i][j]=0;
        }
    }
    for (int i=0; i<N; i++) {
        alpha[i][edgeType[i]]=1;
        beta[i][0]=1;
        if (edgeType[i]>2) {
            if (i==0) i1=N-1;
            else i1=i-1;
            if (i==N-1) i2=0;
            else i2=i+1;
            if (edgeType[i1]>0 && edgeType[i2]>0) {
                beta[i][edgeType[i]-2]=1;
                beta[i][0]=0;
            } else if (edgeType[i2]>0) {
                alpha[i][6]=1;
                alpha[i][edgeType[i]]=0;
                beta[i][edgeType[i]-2]=1;
                beta[i][0]=0;
            }
        }
    }
}
void mpfao::setBoundaryCVPh(void)
{
    int numOfEq = 2*N;
    for (int i=0; i<N; i++) {
        if (alpha[i][6]==1)	numOfEq--;
        if (beta[i][0]==1)	numOfEq--;
        if (alpha[i][0]==1)	numOfEq--;
    }
    MATRIX A, B, C;
    A.SetNEl(numOfEq,numOfEq,1);
    B.SetNEl(N+1,numOfEq,1);
    C.SetNEl(N+1,numOfEq,1);
    A.SetData(0.0);
    B.SetData(0.0);
    int i1, i2, i3, i4;
    double temp;
    int l=0;
    for (int i=0; i<N; i++) {
        if (i==0) i1=numOfEq-1;
        else i1=2*i-1-l;
        if (i==N-1) i2=0;
        else i2=2*i+2-l;
        if (i==N-1) i3=0;
        else i3=i+1;
        i4 = 2*i+1;
        if(alpha[i][6]==1 || beta[i][0]==1) {
            if (i!=N-1) i2--;
            i4--;
        }
        if (alpha[i][0]!=1 && alpha[i][6]!=1) {
            if ((alpha[i][1]+alpha[i][3]+alpha[i][4])==1.) {
                temp = A.GetData(i1, 2*i-l, 0)+G->GetData(2*i,0,0);
                A.SetData(i1,	2*i-l,	0,	temp);
            }
            temp = A.GetData(2*i-l, 2*i-l, 0)+alpha[i][2]+alpha[i][5];
            temp += -G->GetData(2*i,1,0)*(alpha[i][1]+alpha[i][3]);
            temp += (-G->GetData(2*i,1,0)+b[i]*sigma[i])*alpha[i][4];
            A.SetData(2*i-l,	2*i-l,	0,	temp);
            if (alpha[i][1]==1.) {
                temp = A.GetData(i4-l, 2*i-l, 0)-G->GetData(2*i+1,0,0);
                A.SetData(i4-l,	2*i-l,	0,	temp);
            }
            if (alpha[i][1]==1.) {
                temp = A.GetData(i2, 2*i-l, 0)+G->GetData(2*i+1,1,0);
                A.SetData(i2,	2*i-l,	0,	temp);
            }
            temp = B.GetData(i, 2*i-l, 0)+(G->GetData(2*i,0,0)-G->GetData(2*i,1,0))*(alpha[i][1]+alpha[i][3]+alpha[i][4]);
            temp += alpha[i][5];
            B.SetData(i, 	2*i-l, 	0, 	temp);
            temp = B.GetData(i3, 2*i-l, 0)+(-G->GetData(2*i+1,0,0)+G->GetData(2*i+1,1,0))*alpha[i][1];
            B.SetData(i3,	2*i-l,	0, 	temp);
            temp = B.GetData(N, 2*i-l, 0)+c[i]*(alpha[i][2]+(alpha[i][3]+alpha[i][4])*sigma[i]);
            B.SetData(N, 	2*i-l, 	0, 	temp);
        } else {
            l++;
        }
        if(alpha[i][6]==1 || beta[i][0]==1) {
            i4++;
        }
        if (beta[i][0]!=1) {
            temp = A.GetData(i4-l, i4-l, 0)-beta[i][0]+G->GetData(2*i+1,0,0)*(beta[i][1]+beta[i][2]);
            temp += b[i]*beta[i][2]+beta[i][3];
            A.SetData(i4-l,	i4-l, 	0, 	temp);
            temp = A.GetData(i2, i4-l, 0)-G->GetData(2*i+1,1,0)*(beta[i][1]+beta[i][2]);
            A.SetData(i2,	i4-l,	0,	temp);

            temp = B.GetData(i3, i4-l, 0)+(G->GetData(2*i+1,0,0)-G->GetData(2*i+1,1,0))*(beta[i][1]+beta[i][2]);
            temp += beta[i][3];
            B.SetData(i3,	i4-l, 	0, 	temp);
            temp = B.GetData(N, i4-l, 0)+c[i]*(beta[i][1]+beta[i][2])*sigma[i];
            B.SetData(N, 	i4-l, 	0,	temp);
        } else {
            l++;
        }
    }
    A = A.Inverse2D();
    C = A*B;
    bool continP=true;
    for (int i=0; i<N; i++)
        if(alpha[i][6]!=1 && beta[i][0]!=1)
            continP=false;
    if (continP) {
        ph->SetNEl(N,N+1,1);
        l=0;
        for(int i=0; i<N; i++) {
            if (alpha[i][0]==1) {
                l++;
                for(int j=0; j<N+1; j++)
                    ph->SetData(i,j,0,0.0);
            } else {
                for(int j=0; j<N+1; j++)
                    ph->SetData(i,j,0,C.GetData(j,i-l,0));
            }
        }
    } else {
        ph->SetNEl(2*N,N+1,1);
        l=0;
        for(int i=0; i<N; i++) {
            if (alpha[i][0]==1) {
                for(int j=0; j<N+1; j++) {
                    ph->SetData(2*i,j,0,0.0);
                    ph->SetData(2*i+1,j,0,0.0);
                }
            } else if (alpha[i][6]==1 || beta[i][0]==1) {
                for(int j=0; j<N+1; j++) {
                    ph->SetData(2*i,j,0,C.GetData(j,l,0));
                    ph->SetData(2*i+1,j,0,C.GetData(j,l,0));
                }
                l++;
            } else {
                for(int j=0; j<N+1; j++)
                    ph->SetData(2*i,j,0,C.GetData(j,l,0));
                l++;
                for(int j=0; j<N+1; j++)
                    ph->SetData(2*i+1,j,0,C.GetData(j,l,0));
                l++;
            }
        }
    }
}
void mpfao::setBoundaryCVFlux(MATRIX* auxG, MATRIX* auxFlux)
{
    MATRIX A, B, C;
    A.SetNEl(N+1,2*N,1);
    B.SetNEl(2*N,2*N,1);
    C.SetNEl(N+1,2*N,1);
    A.SetData(0.0);
    B.SetData(0.0);
    C.SetData(0.0);
    int i1, i2, i3;
    for (int i=0; i<N; i++) {
        if(i==0) i1=2*N-1;
        else i1=2*i-1;
        if(i==N-1) i2=0;
        else i2=2*i+2;
        if (i==N-1) i3=0;
        else i3=i+1;
        A.SetData(i,	2*i,	0,	-auxG->GetData(2*i,0,0)+auxG->GetData(2*i,1,0));
        B.SetData(i1,	2*i,	0, 	 auxG->GetData(2*i,0,0));
        B.SetData(2*i,	2*i,	0, 	-auxG->GetData(2*i,1,0));
        A.SetData(i3,		2*i+1,	0,	-auxG->GetData(2*i+1,0,0)+auxG->GetData(2*i+1,1,0));
        B.SetData(2*i+1,	2*i+1,	0, 	 auxG->GetData(2*i+1,0,0));
        B.SetData(i2,		2*i+1,	0, 	-auxG->GetData(2*i+1,1,0));
        if(ph->GetNEl()==N*(N+1)) {
            for (int j=0; j<N+1; j++) {
                C.SetData(j,2*i,0,ph->GetData(i,j,0));
                C.SetData(j,2*i+1,0,ph->GetData(i,j,0));
            }
        } else {
            for (int j=0; j<N+1; j++) {
                C.SetData(j,2*i,0,ph->GetData(2*i,j,0));
                C.SetData(j,2*i+1,0,ph->GetData(2*i+1,j,0));
            }
        }
    }
    C = A+B*C;
    bool doubleFluxes = false;
    for (int i=0; i<N; i++) {
        if(i==0) i1 = N-1;
        else i1 = i-1;
        if(i==N-1) i2 = 0;
        else i2 = i+1;
        if (edgeType[i]>1 && edgeType[i1]>0 && edgeType[i2]>0)
            doubleFluxes = true;
    }
    if(!doubleFluxes) {
        auxFlux->SetNEl(N,N+1,1);
        for(int i=0; i<N; i++) {
            if (i==0) i1=N-1;
            else i1=i-1;
            for(int j=0; j<N+1; j++) {
                if(edgeType[i]==0)
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i,0));
                else if (edgeType[i1]==0)
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i+1,0));
                else
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i,0));
            }
        }
    } else {
        auxFlux->SetNEl(2*N,N+1,1);
        for(int i=0; i<2*N; i++)
            for(int j=0; j<N+1; j++)
                auxFlux->SetData(i,j,0,C.GetData(j,i,0));
    }
}
void mpfao::setBoundaryCVPhWithoutBC(void)
{
    int numOfEqWithBC = 2*N;
    int numOfEq = 0;
    int numOfBoundUnkn = 0;
    int *boundEdge = new int[2*N];
    int i2, i4, tempi;
    for (int i=0; i<N; i++) {
        if (alpha[i][6]==1)	numOfEqWithBC--;
        if (beta[i][0]==1)	numOfEqWithBC--;
        if (alpha[i][0]==1)	numOfEqWithBC--;
    }
    for (int i=0; i<N; i++) {
        boundEdge[2*i]=0;
        boundEdge[2*i+1]=0;
        if (alpha[i][1]==1)	numOfEq++;
        if (edgeType[i]>1) {
            if (i==0) tempi=N-1; else tempi=i-1;
            if (edgeType[tempi]>0) {
                numOfBoundUnkn++;
                boundEdge[2*i]=numOfBoundUnkn;
            }
            if (i==N-1) tempi=0; else tempi=i+1;
            if (edgeType[tempi]>0) {
                numOfBoundUnkn++;
                boundEdge[2*i+1]=numOfBoundUnkn;
            }
        }
    }
    MATRIX A, B, C;
    A.SetNEl(numOfEq,numOfEq,1);
    B.SetNEl(N+numOfBoundUnkn,numOfEq,1);
    C.SetNEl(N+numOfBoundUnkn,numOfEq,1);
    A.SetData(0.0);
    B.SetData(0.0);
    C.SetData(0.0);
    double temp;
    int l=0;
    for (int i=0; i<N; i++) {
        if (i==N-1) i2=0; else i2=2*i+2-l;
        i4 = 2*i+1;
        if(alpha[i][6]==1 || beta[i][0]==1) {
            if (i!=N-1) i2--;
            i4--;
        }
        if (alpha[i][1]==1) {
            if (i==0) tempi=2*N-1; else tempi=2*i-1;
            if (boundEdge[tempi]!=0) {
                temp = B.GetData(N+boundEdge[tempi]-1, 2*i-l, 0)-G->GetData(2*i,0,0);
                B.SetData(N+boundEdge[tempi]-1, 2*i-l, 0, temp);
            } else {
                if (i==0) tempi=numOfEq-1; else tempi=2*i-1-l;
                temp = A.GetData(tempi, 2*i-l, 0)+G->GetData(2*i,0,0);
                A.SetData(tempi,	2*i-l,	0,	temp);
            }
            temp = A.GetData(2*i-l, 2*i-l, 0)-G->GetData(2*i,1,0);
            A.SetData(2*i-l,	2*i-l,	0,	temp);
            temp = A.GetData(i4-l, 2*i-l, 0)-G->GetData(2*i+1,0,0);
            A.SetData(i4-l,	2*i-l,	0,	temp);
            if (i==N-1) tempi=0; else tempi=2*i+2;
            if(boundEdge[tempi]!=0) {
                temp = B.GetData(N+boundEdge[tempi]-1, 2*i-l, 0)-G->GetData(2*i+1,1,0);
                B.SetData(N+boundEdge[tempi]-1,	2*i-l,	0,	temp);
            } else {
                temp = A.GetData(i2, 2*i-l, 0)+G->GetData(2*i+1,1,0);
                A.SetData(i2, 2*i-l, 0, temp);
            }

            temp = B.GetData(i, 2*i-l, 0)+(G->GetData(2*i,0,0)-G->GetData(2*i,1,0));
            B.SetData(i, 	2*i-l, 	0, 	temp);

            if (i==N-1) tempi=0; else tempi=i+1;
            temp = B.GetData(tempi, 2*i-l, 0)+(-G->GetData(2*i+1,0,0)+G->GetData(2*i+1,1,0));
            B.SetData(tempi,	2*i-l,	0, 	temp);
        } else {
            l++;
        }
        if(alpha[i][1]!=1) {
            i4++;
        }
        l++;
    }
    A = A.Inverse2D();
    C = A*B;
    bool continP=true;
    for (int i=0; i<N; i++)
        if(alpha[i][6]!=1 && beta[i][0]!=1)
            continP=false;
    if (continP) {
        ph->SetNEl(N,N+numOfBoundUnkn,1);
        l=0;
        for(int i=0; i<N; i++) {
            if (alpha[i][1]!=1) {
                l++;
                for(int j=0; j<N+numOfBoundUnkn; j++)
                    ph->SetData(i,j,0,0.0);
            } else {
                for(int j=0; j<N+numOfBoundUnkn; j++)
                    ph->SetData(i,j,0,C.GetData(j,i-l,0));
            }
        }
    } else {
        ph->SetNEl(2*N,N+numOfBoundUnkn,1);
        l=0;
        for(int i=0; i<N; i++) {
            if (alpha[i][1]!=1) {
                for(int j=0; j<N+numOfBoundUnkn; j++) {
                    ph->SetData(2*i,j,0,0.0);
                    ph->SetData(2*i+1,j,0,0.0);
                }
            } else {
                for(int j=0; j<N+numOfBoundUnkn; j++)
                    ph->SetData(2*i,j,0,C.GetData(j,l,0));
                for(int j=0; j<N+numOfBoundUnkn; j++)
                    ph->SetData(2*i+1,j,0,C.GetData(j,l,0));
                l++;
            }
        }
    }
    delete []boundEdge;
}
void mpfao::setBoundaryCVFluxWithoutBC(MATRIX* auxG, MATRIX* auxFlux)
{
    int numOfEqWithBC = 2*N;
    int numOfEq = 0;
    int numOfBoundUnkn = 0;
    int *boundEdge = new int[2*N];
    int i1, i2, i3;
    for (int i=0; i<N; i++) {
        if (alpha[i][6]==1)	numOfEqWithBC--;
        if (beta[i][0]==1)	numOfEqWithBC--;
        if (alpha[i][0]==1)	numOfEqWithBC--;
    }
    for (int i=0; i<N; i++) {
        boundEdge[2*i]=0;
        boundEdge[2*i+1]=0;
        if (alpha[i][1]==1)	numOfEq++;
        if (edgeType[i]>1) {
            if (i==0) i1=N-1; else i1=i-1;
            if (edgeType[i1]>0) {
                numOfBoundUnkn++;
                boundEdge[2*i]=numOfBoundUnkn;
            }
            if (i==N-1) i1=0; else i1=i+1;
            if (edgeType[i1]>0) {
                numOfBoundUnkn++;
                boundEdge[2*i+1]=numOfBoundUnkn;
            }
        }
    }
    MATRIX A, B, C;
    A.SetNEl(N+numOfBoundUnkn,2*N,1);
    B.SetNEl(2*N,2*N,1);
    C.SetNEl(N+numOfBoundUnkn,2*N,1);
    A.SetData(0.0);
    B.SetData(0.0);
    C.SetData(0.0);
    for (int i=0; i<N; i++) {
        if(i==0) i1=2*N-1; else i1=2*i-1;
        if(i==N-1) i2=0; else i2=2*i+2;
        if (i==N-1) i3=0; else i3=i+1;
        A.SetData(i,	2*i,	0,	-auxG->GetData(2*i,0,0)+auxG->GetData(2*i,1,0));
        if (boundEdge[i1]!=0) {
            A.SetData(N+boundEdge[i1]-1, 2*i, 0, auxG->GetData(2*i,0,0));
        } else {
            B.SetData(i1,	2*i,	0, 	 auxG->GetData(2*i,0,0));
        }
        if (boundEdge[2*i]!=0) {
            A.SetData(N+boundEdge[2*i]-1, 2*i, 0, -auxG->GetData(2*i,1,0));
        } else {
            B.SetData(2*i,	2*i,	0, 	-auxG->GetData(2*i,1,0));
        }
        A.SetData(i3,		2*i+1,	0,	-auxG->GetData(2*i+1,0,0)+auxG->GetData(2*i+1,1,0));
        if (boundEdge[2*i+1]!=0) {
            A.SetData(N+boundEdge[2*i+1]-1, 2*i+1, 0, auxG->GetData(2*i+1,0,0));
        } else {
            B.SetData(2*i+1,	2*i+1,	0, 	 auxG->GetData(2*i+1,0,0));
        }
        if (boundEdge[i2]!=0) {
            A.SetData(N+boundEdge[i2]-1, 2*i+1, 0, -auxG->GetData(2*i+1,1,0));
        } else {
            B.SetData(i2,		2*i+1,	0, 	-auxG->GetData(2*i+1,1,0));
        }
        if(ph->GetNEl()==N*(N+numOfBoundUnkn)) {
            for (int j=0; j<N+numOfBoundUnkn; j++) {
                C.SetData(j,2*i,0,ph->GetData(i,j,0));
                C.SetData(j,2*i+1,0,ph->GetData(i,j,0));
            }
        } else {
            for (int j=0; j<N+numOfBoundUnkn; j++) {
                C.SetData(j,2*i,0,ph->GetData(2*i,j,0));
                C.SetData(j,2*i+1,0,ph->GetData(2*i+1,j,0));
            }
        }
    }
    delete [] boundEdge;
    C = A+B*C;
    bool doubleFluxes = false;
    for (int i=0; i<N; i++) {
        if(i==0) i1 = N-1;
        else i1 = i-1;
        if(i==N-1) i2 = 0;
        else i2 = i+1;
        if (edgeType[i]>1 && edgeType[i1]>0 && edgeType[i2]>0)
            doubleFluxes = true;
    }
    if(!doubleFluxes) {
        auxFlux->SetNEl(N,N+numOfBoundUnkn,1);
        for(int i=0; i<N; i++) {
            if (i==0) i1=N-1;
            else i1=i-1;
            for(int j=0; j<N+numOfBoundUnkn; j++) {
                if(edgeType[i]==0)
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i,0));
                else if (edgeType[i1]==0)
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i+1,0));
                else
                    auxFlux->SetData(i,j,0,C.GetData(j,2*i,0));
            }
        }
    } else {
        auxFlux->SetNEl(2*N,N+numOfBoundUnkn,1);
        for(int i=0; i<2*N; i++)
            for(int j=0; j<N+numOfBoundUnkn; j++)
                auxFlux->SetData(i,j,0,C.GetData(j,i,0));
    }
}
void mpfao::normal(double x1, double y1, double x2, double y2, double *nx, double *ny)
{
    /*!
  \details

  This method computes the normal vector to the segment AB, where A = (x1,y1), B = (x2,y2).
  The length of the normal equals to the length of segment AB.
  Normal vector points always upwards or if segment is vertical than to the right.

  @param[in] x1 x-coordinate of A
  @param[in] y1 y-coordinate of A
  @param[in] x2 x-coordinate of B
  @param[in] y2 y-coordinate of B
  @param[out] *nx pointer to x-coordinate of normal vector
  @param[out] *ny pointer to y-coordinate of normal vector
*/
    double dn1 = pow(pow(x1-x2,2.)+pow(y1-y2,2.),0.5);
    if (dn1<EPS2)
    {
        printf("Error: input segment has zero length!\n");
        *nx = 0.;
        *ny = 0.;
    }
    else if (fabs(x1-x2)<EPS2)
    {
        *nx = dn1;
        *ny = 0.;
    }
    else
    {
        double K = (y2-y1)/(x2-x1);
        if (fabs(K)<EPS2)
        {
            *nx = 0.;
            *ny = dn1;
        }
        else
        {
            double X1 = (x1+x2)/2;
            double Y1 = (y1+y2)/2;
            double X2, Y2;
            if (y2<y1) Y2=y1;
            else Y2=y2;
            X2 = X1-K*(Y2-Y1);
            double dn2 = pow(pow(X2-X1,2)+pow(Y2-Y1,2),0.5);
            *nx = (X2-X1)/dn2*dn1;
            *ny = (Y2-Y1)/dn2*dn1;
        }
    }
}
void mpfao::unitNormal(double x1, double y1, double x2, double y2, double *nx, double *ny)
{
    /*!
  \details

  This method computes the unit normal vector to the segment AB, where A = (x1,y1), B = (x2,y2).
  Normal vector points always upwards or if segment is vertical than to the right.

  @param[in] x1 x-coordinate of A
  @param[in] y1 y-coordinate of A
  @param[in] x2 x-coordinate of B
  @param[in] y2 y-coordinate of B
  @param[out] *nx pointer to x-coordinate of normal vector
  @param[out] *ny pointer to y-coordinate of normal vector
*/
    if (fabs(x1-x2)<EPS2 && fabs(y1-y2)<EPS2)
    {
        printf("Error: input segment has zero length!\n");
        *nx = 0.;
        *ny = 0.;
    }
    else if (fabs(x1-x2)<EPS2)
    {
        *nx = 1.;
        *ny = 0.;
    }
    else
    {
        double K = (y2-y1)/(x2-x1);
        if (fabs(K)<EPS2)
        {
            *nx = 0.;
            *ny = 1.;
        }
        else
        {
            double X1 = (x1+x2)/2;
            double Y1 = (y1+y2)/2;
            double X2, Y2;
            if (y2<y1) Y2=y1;
            else Y2=y2;
            X2 = X1-K*(Y2-Y1);
            double dn = pow(pow(X2-X1,2)+pow(Y2-Y1,2),0.5);
            *nx = (X2-X1)/dn;
            *ny = (Y2-Y1)/dn;
        }
    }
}
void mpfao::outwardVector(double x1, double y1, double x2, double y2, double cx, double cy, double *nx, double *ny)
{
    /*!
  \details

  This method checks direction of the vector (nx,ny). If it is not outward to
  the cell with center (cx, cy) and edge AB, where A = (x1,y1), B = (x2,y2), it is multiplied by -1.

  @param[in] x1 x-coordinate of A
  @param[in] y1 y-coordinate of A
  @param[in] x2 x-coordinate of B
  @param[in] y2 y-coordinate of B
  @param[in] cx x-coordinate of cell center
  @param[in] cy y-coordinate of cell center
 @param[out] *nx pointer to x-coordinate of normal vector
  @param[out] *ny pointer to y-coordinate of normal vector
*/
    if (((x1+x2)/2-cx)*(*nx) + ((y1+y2)/2-cy)*(*ny)<0) {
        *nx *= -1;
        *ny *= -1;
    }
}
void mpfao::inwardVector(double x1, double y1, double x2, double y2, double cx, double cy, double *nx, double *ny)
{
    /*!
  \details

  This method checks direction of the vector (nx,ny). If it is not inward to
  the cell with center (cx, cy) and edge AB, where A = (x1,y1), B = (x2,y2), it is multiplied by -1.

  @param[in] x1 x-coordinate of A
  @param[in] y1 y-coordinate of A
  @param[in] x2 x-coordinate of B
  @param[in] y2 y-coordinate of B
  @param[in] cx x-coordinate of cell center
  @param[in] cy y-coordinate of cell center
 @param[out] *nx pointer to x-coordinate of normal vector
  @param[out] *ny pointer to y-coordinate of normal vector
*/
    if (((x1+x2)/2-cx)*(*nx) + ((y1+y2)/2-cy)*(*ny)>0) {
        *nx *= -1;
        *ny *= -1;
    }
}
