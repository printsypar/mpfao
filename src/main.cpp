#include<time.h>
#include<vector>
#include<iostream>

#include <../src/mpfao.h>

int main(int argc, char *argv[])
{
    // Setting coordinates
    MATRIX coord;
    coord.SetNEl(9,2,1);
    coord.SetData(0,0,0,1.0);    coord.SetData(0,1,0,1.0);
    coord.SetData(1,0,0,0.5);    coord.SetData(1,1,0,1.5);
    coord.SetData(2,0,0,0.5);    coord.SetData(2,1,0,1.0);
    coord.SetData(3,0,0,0.5);    coord.SetData(3,1,0,0.5);
    coord.SetData(4,0,0,1.0);    coord.SetData(4,1,0,0.5);
    coord.SetData(5,0,0,1.5);    coord.SetData(5,1,0,0.5);
    coord.SetData(6,0,0,1.5);    coord.SetData(6,1,0,1.0);
    coord.SetData(7,0,0,1.5);    coord.SetData(7,1,0,1.5);
    coord.SetData(8,0,0,1.0);    coord.SetData(8,1,0,1.5);

    MATRIX perm;
    perm.SetNEl(4,2,2);
    perm.SetData(0,0,0,1.); perm.SetData(0,0,1,0.); perm.SetData(0,1,0,0.); perm.SetData(0,1,1,1.);
    perm.SetData(1,0,0,1.); perm.SetData(1,0,1,0.); perm.SetData(1,1,0,0.); perm.SetData(1,1,1,1.);
    perm.SetData(2,0,0,1.); perm.SetData(2,0,1,0.); perm.SetData(2,1,0,0.); perm.SetData(2,1,1,1.);
    perm.SetData(3,0,0,1.); perm.SetData(3,0,1,0.); perm.SetData(3,1,0,0.); perm.SetData(3,1,1,1.);

    int edgeType[4];
    mpfao discr;
    std::vector<double> vec(2);
    MATRIX projectedFlux;

    /// INTERNAL CONTROL VOLUME
    cout << endl << endl << "*** INTERNAL CONTROL VOLUME ***" << endl;

    // setting edge types
    edgeType[0] = 1;  //all internal egdes
    edgeType[1] = 1;
    edgeType[2] = 1;
    edgeType[3] = 1;

    // How to use
    discr.init(coord, perm, edgeType);
    discr.runMPFAComputations();
    // for internal edges there is not difference
    // between runMPFAComputations and runMPFAComputationsWithoutBC

    cout << "========================================================== " << endl;
    cout << "===== ROWs - unknowns in the center half edges       ===== " << endl;
    cout << "===== COLUMNs - unknowns in the cell centers         ===== " << endl;
    cout << "=====         + the last column for constant from BC ===== " << endl;
    cout << "========================================================== " << endl;

    cout << "Approximation of unknowns on boundary edges: " << endl;
    discr.getPh().print();
    cout << endl;

    cout << "Integrals of the normal fluxes over all half edges in the control volume: " << endl;
    discr.getFlux().print();
    cout << endl;

    vec[0] = 0.05;
    vec[1] = 0.01;
    cout << "Fluxes projected to vector (" << vec[0] << ", " << vec[1] << "):" << endl;
    projectedFlux = discr.projectFluxToVector(vec[0], vec[1]);
    projectedFlux.print();
    // to get (i,j) component of the flux call GetData(i,j,0):
    //projectedFlux.GetData(0,1,0);

    /// BOUNDARY CONTROL VOLUME
    cout << endl << endl << "*** BOUNDARY CONTROL VOLUME ***" << endl;

    edgeType[0] = 1;  // internal egde
    edgeType[1] = 2;  // boundary edge with Dirichlet BC
    edgeType[2] = 0;  // fake edge
    edgeType[3] = 3;  // boundary edge with Neumann BC

    double c[] = {0., 13, 0., 0.};

    discr.init(coord, perm, edgeType, c);

    /// With boundary conditions included in MPFA
    discr.runMPFAComputations();

    cout << endl << "========================================================== " << endl;
    cout << "===== ROWs - unknowns in the center half edges       ===== " << endl;
    cout << "===== COLUMNs - unknowns in the cell centers         ===== " << endl;
    cout << "=====         + the last column for constant from BC ===== " << endl;
    cout << "========================================================== " << endl;

    cout << "Approximation of unknowns on boundary edges: " << endl;
    discr.getPh().print();
    cout << endl;

    cout << "Integrals of the normal fluxes over all half edges in the control volume: " << endl;
    discr.getFlux().print();
    cout << endl;

    vec[0] = 0.05;
    vec[1] = 0.01;
    cout << "Fluxes projected to vector (" << vec[0] << ", " << vec[1] << "):" << endl;
    projectedFlux = discr.projectFluxToVector(vec[0], vec[1]);
    projectedFlux.print();

    /// With new unknowns on the boundary egdes
    // for this method array c[] is not needed in init()
    discr.runMPFAComputationsWithoutBC();

    cout << endl << "========================================================== " << endl;
    cout << "===== ROWs - unknowns in the center half edges       ===== " << endl;
    cout << "===== COLUMNs - unknowns in the cell centers         ===== " << endl;
    cout << "=====         + unknowns on the boundary egdes       ===== " << endl;
    cout << "========================================================== " << endl;

    cout << "Approximation of unknowns on boundary edges: " << endl;
    discr.getPh().print();
    cout << endl;

    cout << "Integrals of the normal fluxes over all half edges in the control volume: " << endl;
    discr.getFlux().print();
    cout << endl;

    vec[0] = 0.05;
    vec[1] = 0.01;
    cout << "Fluxes projected to vector (" << vec[0] << ", " << vec[1] << "):" << endl;
    projectedFlux = discr.projectFluxToVectorWithoutBC(vec[0], vec[1]);
    projectedFlux.print();


    return 0;
}
