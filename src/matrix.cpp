#include"matrix.h"
//-----------------------------------------------------------------------------
MATRIX::MATRIX()
{
    nEl[0] = nEl[1] = nEl[2] = 0;
}
//-----------------------------------------------------------------------------
MATRIX::MATRIX(const int *_nEl)
{
    nEl[0]     = _nEl[0];
    nEl[1]     = _nEl[1];
    nEl[2]     = _nEl[2];
    data =std::vector<double>(_nEl[0]*_nEl[1]*_nEl[2]);
}
//-----------------------------------------------------------------------------
MATRIX::MATRIX(const int *_nEl, const double val)
{
    nEl[0]     = _nEl[0];
    nEl[1]     = _nEl[1];
    nEl[2]     = _nEl[2];
    data =std::vector<double>(_nEl[0]*_nEl[1]*_nEl[2], val);
}
//-----------------------------------------------------------------------------
MATRIX::MATRIX(const int *_nEl, const double *val)
{
    nEl[0]     = _nEl[0];
    nEl[1]     = _nEl[1];
    nEl[2]     = _nEl[2];
    data =std::vector<double>(_nEl[0]*_nEl[1]*_nEl[2]);
    for (int i=0; i<_nEl[0]*_nEl[1]*_nEl[2]; i++)
        data[i] = val[i];
}
//-----------------------------------------------------------------------------
MATRIX::MATRIX(const MATRIX& m)
{
    nEl[0] = m.GetNEl(0);
    nEl[1] = m.GetNEl(1);
    nEl[2] = m.GetNEl(2);
    data =std::vector<double>(m.GetNEl());
    for (int i=0;i<m.GetNEl();i++)
        data[i] = m.GetData(i);
}
//-----------------------------------------------------------------------------
MATRIX& MATRIX::operator = (const MATRIX &m)
{
    if (this != &m) {
        nEl[0] = m.GetNEl(0);
        nEl[1] = m.GetNEl(1);
        nEl[2] = m.GetNEl(2);
        data = m.GetData();
    }
    return *this;
}//MATRIX& MATRIX::operator = (const MATRIX &)
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator + (const MATRIX &m) const
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator + : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator + : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator + : number of elements in z-direction is not equal");

    MATRIX newm(m);

    for (int i=0; i<GetNEl(); i++)
        newm.data[i] = newm.data[i]+data[i];

    return newm;
}//MATRIX MATRIX::operator + (const MATRIX &) const
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator + (const double val)
{
    MATRIX newm(GetNElCmp());

    for (int i=0; i<GetNEl(); i++)
        newm.data[i] = data[i]+val;

    return newm;
}//MATRIX MATRIX::operator + (const double)
//-----------------------------------------------------------------------------
MATRIX& MATRIX::operator += (const MATRIX& m)
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator += : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator += : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator += : number of elements in z-direction is not equal");

    for (int i=0; i<GetNEl(); i++)
        data[i] += m.data[i];

    return *this;
}//MATRIX& MATRIX::operator += (const MATRIX&)
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator - (const MATRIX &m) const
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator - : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator - : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator - : number of elements in z-direction is not equal");

    MATRIX newm(m);

    for (int i=0; i<GetNEl(); i++)
        newm.data[i] = data[i]-newm.data[i];

    return newm;
}//MATRIX MATRIX::operator - (const MATRIX &) const
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator - (const double val)const
{
    MATRIX newm(GetNElCmp());

    for (int i=0; i<GetNEl(); i++)
        newm.data[i] = data[i]-val;

    return newm;
}//MATRIX MATRIX::operator - (const double)
//-----------------------------------------------------------------------------
MATRIX& MATRIX::operator -= (const MATRIX& m)
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator -= : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator -= : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator -= : number of elements in z-direction is not equal");

    for (int i=0; i<GetNEl(); i++)
        data[i] -= m.data[i];

    return *this;
}//MATRIX& MATRIX::operator -= (const MATRIX&)
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator * (const double val)const
{
    MATRIX newm(GetNElCmp());

    for (int i=0; i<GetNEl(); i++)
        newm.data[i] = data[i]*val;

    return newm;
}//MATRIX MATRIX::operator * (const double)
//-----------------------------------------------------------------------------
MATRIX MATRIX::operator * (const MATRIX& m)
{
    if (GetNEl(2)!=1 || m.GetNEl(2)!=1)
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator * : this method works only for 2D matrixes");
    if (GetNEl(0)!=m.GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator * : sizes of matrixes are inconsistent");
    MATRIX M;
    M.SetNEl(m.GetNEl(0),GetNEl(1),1);
    double temp;
    for (int i=0; i<m.GetNEl(0); i++)
        for (int j=0; j<GetNEl(1); j++)
        {
            temp = 0.;
            for (int k=0; k<GetNEl(0); k++)
                temp += GetData(k,j,int(0))*m.GetData(i,k,int(0));
            M.SetData(i,j,int(0),temp);
        }
    return M;
}//MATRIX MATRIX::operator * (const MATRIX&)
//-----------------------------------------------------------------------------
bool MATRIX::operator == (MATRIX& m)
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator == : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator == : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator == : number of elements in z-direction is not equal");
    return (data==m.data);
}//bool MATRIX::operator == (MATRIX&)
//-----------------------------------------------------------------------------
bool MATRIX::operator != (MATRIX& m)
{
    if (m.GetNEl(0)!=GetNEl(0))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator != : number of elements in x-direction is not equal");
    if (m.GetNEl(1)!=GetNEl(1))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator != : number of elements in y-direction is not equal");
    if (m.GetNEl(2)!=GetNEl(2))
        ERRORHANDLER::Instance().Exit
                ("MATRIX::operator != : number of elements in z-direction is not equal");
    return (data==m.data);
}//bool MATRIX::operator != (MATRIX&)
//-----------------------------------------------------------------------------
double MATRIX::determinant2D22(void)const
{
    if (nEl[0]!=2 || nEl[1]!=2 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::determinant2D22: the function can not be applied to this matrix");
    return data[0]*data[3]-data[1]*data[2];
}
//-----------------------------------------------------------------------------
double MATRIX::determinant2D33(void)const
{
    if (nEl[0]!=3 || nEl[1]!=3 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::determinant2D33: the function can not be applied to this matrix");
    double det=data[0]*(data[4]*data[8]-data[5]*data[7]);
    det=det-data[1]*(data[3]*data[8]-data[5]*data[6]);
    det=det+data[2]*(data[3]*data[7]-data[4]*data[6]);
    return det;
}
//-----------------------------------------------------------------------------
double MATRIX::determinant2D44(void)const
{
    if (nEl[0]!=4 || nEl[1]!=4 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::determinant2D44: the function can not be applied to this matrix");
    double DET=0.0;
    MATRIX A;
    A.SetNEl(3,3,1);
    for (int i=0; i<4; i++)
    {
        int l=0;
        for (int x1=0; x1<4; x1++)
            if(x1!=i) {
                for (int x2=0; x2<3; x2++)
                    A.SetData(l,x2,int(0),this->GetData(x1,x2+1,int(0)));
                l++;
            }
        DET+=pow(-1.0,(int)i)*this->GetData(i,int(0),int(0))*A.determinant2D33();
    }
    return DET;
}
//-----------------------------------------------------------------------------
double MATRIX::determinant2D(void)const
{
    if (nEl[0]!=nEl[1] || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::determinant2D: the function can not be applied to this matrix");
    double DET=0.0;
    int N = nEl[0];
    if (N==3) return this->determinant2D33();
    else if (N==2) return this->determinant2D22();
    else if (N==1) return fabs(this->GetData(0,0,0));
    MATRIX A;
    A.SetNEl(N-1,N-1,1);
    for (int i=0; i<N; i++)
    {
        int l=0;
        for (int x1=0; x1<N; x1++)
            if(x1!=i) {
                for (int x2=0; x2<N-1; x2++)
                    A.SetData(l,x2,int(0),this->GetData(x1,x2+1,int(0)));
                l++;
            }
        if (N==4)
            DET+=pow(-1.0,(int)i)*this->GetData(i,int(0),int(0))*A.determinant2D33();
        else
            DET+=pow(-1.0,(int)i)*this->GetData(i,int(0),int(0))*A.determinant2D();
    }
    return DET;
}
//-----------------------------------------------------------------------------
MATRIX MATRIX::Inverse2D22(void)const
{
    if (nEl[0]!=2 || nEl[1]!=2 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D22: the function can not be applied to this matrix");
    double det=data[0]*data[3]-data[1]*data[2];
    if (det==0) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D22: determinant of matrix nn equal to zero");
    MATRIX inverseNN(GetNElCmp());
    inverseNN.SetData(int(0),data[3]/det);
    inverseNN.SetData(int(1),-data[1]/det);
    inverseNN.SetData(int(2),-data[2]/det);
    inverseNN.SetData(int(3),data[0]/det);
    return inverseNN;
}
//-----------------------------------------------------------------------------
MATRIX MATRIX::Inverse2D33(void)const
{
    if (nEl[0]!=3 || nEl[1]!=3 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D33: the function can not be applied to this matrix");
    double det,count;
    det=data[0]*(data[4]*data[8]-data[5]*data[7]);
    det=det-data[1]*(data[3]*data[8]-data[5]*data[6]);
    det=det+data[2]*(data[3]*data[7]-data[4]*data[6]);
    if (det==0) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D33: determinant of matrix nn equal to zero");
    MATRIX inverseNN(GetNElCmp());
    count=(data[4]*data[8]-data[5]*data[7])/det;
    inverseNN.SetData(int(0),count);
    count=-(data[1]*data[8]-data[2]*data[7])/det;
    inverseNN.SetData(int(1),count);
    count=(data[1]*data[5]-data[2]*data[4])/det;
    inverseNN.SetData(int(2),count);
    count=-(data[3]*data[8]-data[5]*data[6])/det;
    inverseNN.SetData(int(3),count);
    count=(data[0]*data[8]-data[2]*data[6])/det;
    inverseNN.SetData(int(4),count);
    count=-(data[0]*data[5]-data[2]*data[3])/det;
    inverseNN.SetData(int(5),count);
    count=(data[3]*data[7]-data[4]*data[6])/det;
    inverseNN.SetData(int(6),count);
    count=-(data[0]*data[7]-data[1]*data[6])/det;
    inverseNN.SetData(int(7),count);
    count=(data[0]*data[4]-data[1]*data[3])/det;
    inverseNN.SetData(int(8),count);
    return inverseNN;
}
//-----------------------------------------------------------------------------
MATRIX MATRIX::Inverse2D44(void)const
{
    if (nEl[0]!=4 || nEl[1]!=4 || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D44: the function can not be applied to this matrix");
    MATRIX newm(GetNElCmp());
    double DET=determinant2D44();
    double temp;
    MATRIX A;
    A.SetNEl(3,3,1);
    for (int i=0; i<4; i++)
        for (int j=0; j<4; j++)
        {
            int l1=0;
            for (int x1=0; x1<4; x1++)
                if(x1!=i) {
                    int l2=0;
                    for (int x2=0; x2<4; x2++)
                        if(x2!=j) {
                            A.SetData(l1,l2,int(0),this->GetData(x1,x2,int(0)));
                            l2++;
                        }
                    l1++;
                }
            temp=pow(-1.,(int)(i+j))*A.determinant2D33()/DET;
            newm.SetData(j,i,int(0),temp);
        }
    return newm;
}
//-----------------------------------------------------------------------------
MATRIX MATRIX::Inverse2D(void)const
{
    if (nEl[0]!=nEl[1] || nEl[2]!=1) ERRORHANDLER::Instance().Exit
            ("MATRIX::Inverse2D: the function can not be applied to this matrix");
    if (nEl[0]==0) return *this;
    if (nEl[0]==1) {
        MATRIX m;
        m.SetNEl(1,1,1);
        m.SetData((int)0,1./data[0]);
        return m;
    }
    if (nEl[0]==2) return this->Inverse2D22();
    if (nEl[0]==3) return this->Inverse2D33();
    if (nEl[0]==4) return this->Inverse2D44();
    MATRIX newm(GetNElCmp());
    double DET = determinant2D();
    double temp = 0.0;
    int N = nEl[0];
    MATRIX A;
    A.SetNEl(N-1,N-1,1);
    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++)
        {
            int l1=0;
            for (int x1=0; x1<N; x1++)
                if(x1!=i) {
                    int l2=0;
                    for (int x2=0; x2<N; x2++)
                        if(x2!=j) {
                            A.SetData(l1,l2,int(0),this->GetData(x1,x2,int(0)));
                            l2++;
                        }
                    l1++;
                }
            if (N-1>=4)
                temp=pow(-1.,(int)(i+j))*A.determinant2D()/DET;
            else if(N-1==3)
                temp=pow(-1.,(int)(i+j))*A.determinant2D33()/DET;
            else if(N-1==2)
                temp=pow(-1.,(int)(i+j))*A.determinant2D22()/DET;
            newm.SetData(j,i,int(0),temp);
        }
    return newm;
}
//-----------------------------------------------------------------------------
void MATRIX::SetData(const double *val)  
{
    if (GetNEl()==0) ERRORHANDLER::Instance().Exit
            ("MATRIX::SetData: dimensions of matrix is indefinite");
    for (int i=0;i<GetNEl();i++)
        data[i] = val[i];
}
//-----------------------------------------------------------------------------
void MATRIX::SetNEl(const int  _nEl)
{
    //if (GetNEl()!=0) ERRORHANDLER::Instance().Exit
    //("MATRIX::SetNEl: dimensions of matrix have already been defined");
    nEl[0]=_nEl;
    nEl[1]=1;
    nEl[2]=1;
    data = std::vector<double>(_nEl);
}
//-----------------------------------------------------------------------------
void MATRIX::SetNEl(const int  *_nEl)
{
    //  if (GetNEl()!=0) ERRORHANDLER::Instance().Exit
    //  ("MATRIX::SetNEl: dimensions of matrix have already been defined");
    if (_nEl[0]!=nEl[0] || _nEl[1]!=nEl[1] || _nEl[2]!=nEl[2]) {
        if (data.size() != 0) {

            int minNx, minNy, minNz;
            if(nEl[0]<_nEl[0]) minNx = nEl[0];
            else minNx = _nEl[0];
            if(nEl[1]<_nEl[1]) minNy = nEl[1];
            else minNy = _nEl[1];
            if(nEl[1]<_nEl[1]) minNz = nEl[2];
            else minNz = _nEl[1];

            std::vector<double> newData(_nEl[0]*_nEl[1]*_nEl[2]);
            for (int i=0; i<minNx; i++)
                for (int j=0; j<minNy; j++)
                    for (int k=0; k<minNz; k++)
                        newData[(i*_nEl[1]+j)*_nEl[2]+k]=data[(i*nEl[1]+j)*nEl[2]+k];

            data = newData;
        } else {
            data = std::vector<double>(_nEl[0]*_nEl[1]*_nEl[2]);
        }
        nEl[0]=_nEl[0];
        nEl[1]=_nEl[1];
        nEl[2]=_nEl[2];
    }
}
//-----------------------------------------------------------------------------
void MATRIX::SetNEl(const int  Nx, const int  Ny, const int  Nz)
{
    //  if (GetNEl()!=0) ERRORHANDLER::Instance().Exit
    //  ("MATRIX::SetNEl: dimensions of matrix have already been defined");
    if (Nx!=nEl[0] || Ny!=nEl[1] || Nz!=nEl[2]) {
        if (data.size() != 0) {
            int minNx, minNy, minNz;
            if(nEl[0]<Nx) minNx = nEl[0];
            else minNx = Nx;
            if(nEl[1]<Ny) minNy = nEl[1];
            else minNy = Ny;
            if(nEl[1]<Nz) minNz = nEl[2];
            else minNz = Nz;

            std::vector<double> newData(Nx*Ny*Nz);
            for (int i=0; i<minNx; i++)
                for (int j=0; j<minNy; j++)
                    for (int k=0; k<minNz; k++)
                        newData[(i*Ny+j)*Nz+k]=data[(i*nEl[1]+j)*nEl[2]+k];
            data = newData;

        } else {
            data = std::vector<double>(Nx*Ny*Nz);
        }
        nEl[0]=Nx;
        nEl[1]=Ny;
        nEl[2]=Nz;
    }
}
//-----------------------------------------------------------------------------
void MATRIX::SetData(double val)
{
    for (int i=0; i<nEl[0]*nEl[1]*nEl[2]; i++) data[i] = val;
}
//-----------------------------------------------------------------------------
void MATRIX::SetData(const int elInd, double val)
{
    if (elInd >= nEl[0]*nEl[1]*nEl[2]) ERRORHANDLER::Instance().Exit
            ("MATRIX::SetData: Index exceeds matrix dimention");
    data[elInd] = val;
}
//-----------------------------------------------------------------------------
void MATRIX::SetData(const int *elInd, double val)
{
    if (elInd[0]>=nEl[0] || elInd[1]>=nEl[1] || elInd[2]>=nEl[2])
        ERRORHANDLER::Instance().Exit
                ("MATRIX::SetData: Index exceeds matrix dimention");
    data[(elInd[0]*nEl[1]+elInd[1])*nEl[2]+elInd[2]] = val;
}
//-----------------------------------------------------------------------------
void MATRIX::SetData(int x, int y, int z, double val)
{
    if (x>=nEl[0] || y>=nEl[1] || z>=nEl[2]) ERRORHANDLER::Instance().Exit
            ("MATRIX::SetData: Index exceeds matrix dimention");
    data[(x*nEl[1]+y)*nEl[2]+z] = val;
}
//-----------------------------------------------------------------------------
const std::vector<double>& MATRIX::GetData() const
{
    return data;
}
//-----------------------------------------------------------------------------
int MATRIX::GetNEl(const int dir)const
{
    if (dir!=0 && dir!=1 && dir!=2) ERRORHANDLER::Instance().Exit
            ("MATRIX::GetNEl: wrong direction");
    return nEl[dir];
}
//-----------------------------------------------------------------------------
int MATRIX::GetNEl()const
{
    return nEl[0]*nEl[1]*nEl[2];
}
//-----------------------------------------------------------------------------
const int *MATRIX::GetNElCmp()const
{
    return nEl;
} 
//-----------------------------------------------------------------------------
double MATRIX::GetData(const int elInd)const
{
    if (elInd >= nEl[0]*nEl[1]*nEl[2]) ERRORHANDLER::Instance().Exit
            ("MATRIX::GetData: Index exceeds matrix dimention");
    return data[elInd];
} 
//-----------------------------------------------------------------------------
double MATRIX::GetData(const int *elInd)const
{
    if (elInd[0]>=nEl[0] || elInd[1]>=nEl[1] || elInd[2]>=nEl[2])
        ERRORHANDLER::Instance().Exit
                ("MATRIX::GetData: Index exceeds matrix dimention");
    return data[(elInd[0]*nEl[1]+elInd[1])*nEl[2]+elInd[2]];
}
//-----------------------------------------------------------------------------
double MATRIX::GetData(int x, int y, int z)const
{
    if (x>=nEl[0] || y>=nEl[1] || z>=nEl[2]) ERRORHANDLER::Instance().Exit
            ("MATRIX::GetData: Index exceeds matrix dimention");
    return data[(x*nEl[1]+y)*nEl[2]+z];
}
//-----------------------------------------------------------------------------
void  MATRIX::print(void)const
{
    for (int i=0;i<nEl[0]*nEl[1]*nEl[2];i++)
    {
        printf("%.3le\t",data[i]);
        if (fmod(double(i+1),nEl[1])==0) printf("\n");
    }
}
//-----------------------------------------------------------------------------
void  MATRIX::printToFile(char* filename)const
{
    FILE* f;
    f = fopen(filename, "w");
    for (int i=0;i<nEl[0]*nEl[1]*nEl[2];i++)
    {
        fprintf(f,"%.3le\t",data[i]);
        if (fmod(double(i+1),nEl[1])==0) fprintf(f,"\n");
    }
    fclose(f);
}
