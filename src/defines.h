#ifndef DEFINES_H
#define DEFINES_H

#define EPS2        1e-5

#define XDIR	0
#define YDIR	1
#define ZDIR	2

#define DBG_OUT_STP(str) cout << str << endl; cin.ignore();
#define DBG_OUT(str) cout << str << endl;
#define DBG_STP cin.ignore();
#define OUT_PROGRESS(num, tot) cout << "\r" << num << " of " << tot << "          " << flush;
//DBGOUT_STP(ind==50, "Value" << parameter);

#endif
