#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H
//-----------------------------------------------------------------------------
#include"singleton.h"
//-----------------------------------------------------------------------------
// Usage ERRORHANDLER::Instance().Exit("ABC not okay");
//-----------------------------------------------------------------------------
class ERRORHANDLER : public SINGLETON<ERRORHANDLER>
{
  friend class SINGLETON<ERRORHANDLER>;
 public:
  void Exit(const char *errorDescription);
 protected:
  ERRORHANDLER(){}; // default constructor
};
//-----------------------------------------------------------------------------
#endif
