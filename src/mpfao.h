#ifndef MPFAO_H
#define MPFAO_H

#include<math.h>
#include<stdio.h>
#include<string.h>
#include<iostream>

#include "../src/errorhandler.h"
#include "../src/matrix.h"
#include "../src/defines.h"

using namespace std;

class mpfaInput {

private:

};

class mpfao {

private:
    int N; 		    // number of duals in the control volume
    MATRIX *coord; 	// coordinates of the points forming the control volume  (size (2xN+1)x2)
    // starting with the central vertex, then a cell center
    // and the rest of the points in a counterclockwise direction
    MATRIX *perm; 	// permeability tensors: i (N or 2N) - number of the edge, j - x-components, k - y-components
    int *edgeType;	// (size N) 0 - no edge, 1 - internal egde, 2 - Dirichlet BC, 3 - Neumann BC, 4 - Robin BC, 5 - p_edge = p_cellcenter
    // 0 - no edge, 1 - internal egde; 2 and greater - boundary edge;
    double *b, *c;

    MATRIX *G; 		// coefficients G (size 2Nx2)
    MATRIX *ph;		// pressure at half egdes (size Nx(N+1) or Nx(N+numOfBoundaryEdges) for the case without BC)
    MATRIX *flux; 	// fluxes at half edges (size Nx(N+1) or Nx(N+numOfBoundaryEdges) for the case without BC)
    MATRIX *n; 		// unit normals to egdes pointing in counterclockwise direction (size Nx2)
    MATRIX *Nu; 		// outward normals to control volume edges (length equals to length of corresponding edge) (size 2Nx2)
    double *delta; 	// area of triangles (size N)
    double *sigma; 	// length of the half edges

    int **alpha;
    int **beta;

    void normal(double, double, double, double, double*, double*);
    void unitNormal(double, double, double, double, double*, double*);
    void outwardVector(double, double, double, double, double, double, double*, double*);
    void inwardVector(double, double, double, double, double, double, double*, double*);

public:

    mpfao();
    ~mpfao();
    void 	init(MATRIX&, MATRIX&, int[]);
    void 	init(MATRIX&, MATRIX&, int[], double*);
    void 	init(MATRIX&, MATRIX&, int[], double*, double*);
    void 	init(int, double[], double[], double**, int[], double*);
    //void 	init(TDual,...);
    void 	runMPFAComputations(void);
    void 	runMPFAComputationsWithoutBC(void);
    //void  runMPFAFluxToVector(double, double);
    MATRIX  projectFluxToVector(double, double);
    MATRIX  projectFluxToVectorWithoutBC(double, double);
    void 	updateBC(int[], double*);

    // SET-functions ...
    void 	setNormal(double, double);
    void 	setInnerCVAuxVariables(void);
    void 	setInnerCVG(void);
    //void 	setInnerCVG(double, double);
    void 	setInnerCVG(double, double, MATRIX*);
    void 	setInnerCVPh(void);
    void  setInnerCVFlux(MATRIX*, MATRIX*);
    void 	setBoundaryCVAuxVariables(void);
    void 	setBoundaryCVG(void);
    //void 	setBoundaryCVG(double, double);
    void 	setBoundaryCVG(double, double, MATRIX*);
    void 	setBC(void);
    void 	setBoundaryCVPh(void);
    void 	setBoundaryCVFlux(MATRIX*, MATRIX*);
    void 	setBoundaryCVPhWithoutBC(void);
    void 	setBoundaryCVFluxWithoutBC(MATRIX*, MATRIX*);

    // GET-functions ...
    double getPh(int i, int j) {return ph->GetData(i,j,0);}
    MATRIX& getPh(void) {return *ph;}
    double getFlux(int i, int j) {return flux->GetData(i,j,0);}
    MATRIX& getFlux(void) {return *flux;}
    double getXG(int i) {return G->GetData(i,0,0);}
    double getYG(int i) {return G->GetData(i,1,0);}
    double getG(int i, int j) {return G->GetData(i,j,0);}
    double getPerm(int i, int j, int k){return perm->GetData(i,j,k);}
    int getPermSize(void){return perm->GetNEl(XDIR);}
    double getNorm(int i, int j){return n->GetData(i,j,0);}
    double getSigma(int i){return sigma[i];}
    int getEdgeType(int i){return edgeType[i];}
    void setNEl(int nx, int ny) {ph->SetNEl(nx,ny,1);flux->SetNEl(nx,ny,1);}

};//class mpfao
#endif
