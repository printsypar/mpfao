#-------------------------------------------------
#
# Project created by QtCreator 2013-06-14T13:03:59
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = mpfao
DESTDIR = ../bin
CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app

SOURCES += ../src/main.cpp \
    ../src/mpfao.cpp \
    ../src/matrix.cpp \
    ../src/errorhandler.cpp

HEADERS += \
    ../src/singleton.h \
    ../src/mpfao.h \
    ../src/matrix.h \
    ../src/errorhandler.h \
    ../src/defines.h
